LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE                  := MIMNative-v1
LOCAL_SRC_FILES               := MIMNative/MIMNative.cpp
LOCAL_LDLIBS                  := C:/android/ndk/platforms/android-19/arch-arm/usr/lib/liblog.so
LCOAL_LDLIBS                  := C:/android/ndk/platforms/android-19/arch-arm/usr/lib/libjnigraphics.so
LOCAL_LDLIBS                  += -lz
LOCAL_CPPFLAGS += -fexceptions

include $(BUILD_SHARED_LIBRARY)