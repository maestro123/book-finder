package com.dream.android.mim;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.view.View;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by artyom on 2.4.15.
 */
public class ImageLoadObject implements Runnable, Cloneable {

    public static final String TAG = ImageLoadObject.class.getSimpleName();

    private static final Handler uiHandler = new Handler(Looper.getMainLooper()) {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            ImageLoadObject loadObject = (ImageLoadObject) msg.obj;
            if (loadObject != null) {
                switch (msg.what) {
                    case MSG_POST_START:
                        loadObject.notifyStart();
                        break;
                    case MSG_POST_RESULT:
                        loadObject.notifyFinish();
                        break;
                }
            }
        }

    };

    static {
        uiHandler.getLooper();
    }

    private static final int MSG_POST_START = 0x1;
    private static final int MSG_POST_RESULT = 0x2;

    private MIM mim;
    private Object mDisplayObject;

    private String key;
    private Object object;
    private String path;

    private int width;
    private int height;
    private int PreLoadResource = -1;
    private int BadLoadResource = -1;
    private MIMAbstractMaker mMaker;
    private MIMAbstractDisplayer mDisplayer;
    private MIMAbstractPostMaker mPostMaker;
    private OnImageLoadEventListener mListener;
    private OnPreLoadListener mPreLoadListener;
    private MIMColorAnalyzer mAnalyzer;
    private Bitmap.Config mConfig = Bitmap.Config.RGB_565;

    private int mRetryCount = -1;
    private AtomicBoolean mCleanPrevious;
    private AtomicBoolean mSaveInCache;
    private AtomicBoolean mSaveInDiscCache;
    private AtomicBoolean mPostProcess;
    private AtomicBoolean mAnalyzeColors;
    private AtomicBoolean mAnimationEnable;
    private AtomicBoolean mCacheAnimationEnable;
    private AtomicBoolean mScaleToFit;
    private AtomicBoolean mUseRecycleDrawable;

    private Object mResultObject;
    private Integer[] mAnalyzedColors;

    private boolean isFresh;
    private boolean isCanceled;
    private boolean isFinished;
    private boolean isLoadedFromCache;

    private boolean isObjectExists = true;

    public ImageLoadObject(String path) {
        this.path = path;
        this.key = path;
    }

    public ImageLoadObject(String key, String path) {
        this.key = key;
        this.path = path;
    }

    public ImageLoadObject key(String key) {
        this.key = key;
        return this;
    }

    public String getKey() {
        return key != null ? key.toString() : path;
    }

    public ImageLoadObject object(Object object) {
        this.object = object;
        return this;
    }

    public Object getObject() {
        return object;
    }

    public ImageLoadObject path(String path) {
        this.path = path;
        return this;
    }

    public String getPath() {
        return path;
    }

    public ImageLoadObject width(int width) {
        this.width = width;
        return this;
    }

    public int getWidth() {
        return width;
    }

    public ImageLoadObject height(int height) {
        this.height = height;
        return this;
    }

    public int getHeight() {
        return height;
    }

    public ImageLoadObject size(int width, int height) {
        this.width = width;
        this.height = height;
        return this;
    }

    public ImageLoadObject preLoadResource(int resId) {
        PreLoadResource = resId;
        return this;
    }

    public int getPreLoadResource() {
        return PreLoadResource;
    }

    public ImageLoadObject badLoadResource(int resId) {
        BadLoadResource = resId;
        return this;
    }

    public int getBadLoadResource() {
        return BadLoadResource;
    }

    public ImageLoadObject maker(MIMAbstractMaker maker) {
        this.mMaker = maker;
        return this;
    }

    public MIMAbstractMaker getMaker() {
        return mMaker;
    }

    public ImageLoadObject displayer(MIMAbstractDisplayer displayer) {
        this.mDisplayer = displayer;
        return this;
    }

    public MIMAbstractDisplayer getDisplayer() {
        return mDisplayer;
    }

    public ImageLoadObject postMaker(MIMAbstractPostMaker maker) {
        this.mPostMaker = maker;
        return this;
    }

    public MIMAbstractPostMaker getPostMaker() {
        return mPostMaker;
    }

    public ImageLoadObject listener(OnImageLoadEventListener listener) {
        this.mListener = listener;
        return this;
    }

    public OnImageLoadEventListener getListener() {
        return mListener;
    }

    public ImageLoadObject preLoadListener(OnPreLoadListener listener) {
        mPreLoadListener = listener;
        return this;
    }

    public ImageLoadObject colorAnalyzer(MIMColorAnalyzer analyzer) {
        this.mAnalyzer = analyzer;
        return this;
    }

    public MIMColorAnalyzer getAnalyzer() {
        return mAnalyzer;
    }

    public ImageLoadObject config(Bitmap.Config config) {
        mConfig = config;
        return this;
    }

    public Bitmap.Config getConfig() {
        return mConfig;
    }

    public ImageLoadObject retryCount(int retryCount) {
        this.mRetryCount = retryCount;
        return this;
    }

    public int getRetryCount() {
        return mRetryCount;
    }

    public ImageLoadObject cleanPreviouse(boolean value) {
        mCleanPrevious = new AtomicBoolean(value);
        return this;
    }

    public boolean cleanPreviouse() {
        return fromAtomic(mCleanPrevious);
    }

    public ImageLoadObject cache(boolean value) {
        mSaveInCache = new AtomicBoolean(value);
        return this;
    }

    public boolean saveInCache() {
        return fromAtomic(mSaveInCache);
    }

    public ImageLoadObject diskCache(boolean value) {
        mSaveInDiscCache = new AtomicBoolean(value);
        return this;
    }

    public boolean saveInDiskCache() {
        return fromAtomic(mSaveInDiscCache);
    }

    public ImageLoadObject postProcess(boolean value) {
        mPostProcess = new AtomicBoolean(value);
        return this;
    }

    public boolean postProcess() {
        return fromAtomic(mPostProcess);
    }

    public ImageLoadObject analyze(boolean value) {
        mAnalyzeColors = new AtomicBoolean(value);
        return this;
    }

    public boolean analyze() {
        return fromAtomic(mAnalyzeColors);
    }

    public ImageLoadObject animationEnable(boolean value) {
        mAnimationEnable = new AtomicBoolean(value);
        return this;
    }

    public boolean animationEnable() {
        return fromAtomic(mAnimationEnable);
    }

    public ImageLoadObject cacheAnimationEnable(boolean value) {
        mCacheAnimationEnable = new AtomicBoolean(value);
        return this;
    }

    public boolean cacheAnimationEnable() {
        return fromAtomic(mCacheAnimationEnable);
    }

    public ImageLoadObject scaleToFit(boolean value) {
        if (mScaleToFit == null) {
            mScaleToFit = new AtomicBoolean();
        }
        mScaleToFit.set(value);
        return this;
    }

    public boolean scaleToFit() {
        return fromAtomic(mScaleToFit);
    }

    public ImageLoadObject useRecyclerDrawable(boolean value) {
        mUseRecycleDrawable = new AtomicBoolean(value);
        return this;
    }

    public boolean useRecyclerDrawable() {
        return fromAtomic(mUseRecycleDrawable);
    }

    public ImageLoadObject fresh(boolean value) {
        isFresh = value;
        return this;
    }

    public Integer[] getAnalyzedColors() {
        return mAnalyzedColors;
    }

    public boolean isCanceled() {
        return isCanceled;
    }

    public void setResult(Object object) {
        if (object instanceof ImageLoadObject) {
            ImageLoadObject loadObject = (ImageLoadObject) object;
            mAnalyzedColors = loadObject.getAnalyzedColors();
            mResultObject = loadObject.getResultObject();
        } else if (object instanceof Bitmap || object instanceof RecyclingBitmapDrawable) {
            mResultObject = object;
        } else {
            throw new UnsupportedOperationException("Unknown class for set result");
        }
    }

    public Object getResultObject() {
        return mResultObject;
    }

    public Bitmap getResultBitmap() {
        return mResultObject instanceof Bitmap ? (Bitmap) mResultObject : mResultObject instanceof RecyclingBitmapDrawable
                ? ((RecyclingBitmapDrawable) mResultObject).getBitmap() : null;
    }

    public RecyclingBitmapDrawable makeRecycleDrawable(Resources resources, Bitmap bitmap) {
        return new RecyclingBitmapDrawable(resources, bitmap);
    }

    public void setObjectExists(boolean objectExists) {
        isObjectExists = objectExists;
    }

    public boolean isObjectExists() {
        return isObjectExists;
    }

    public boolean haveValidResult() {
        return mResultObject != null && (mResultObject instanceof RecyclingBitmapDrawable
                ? ((RecyclingBitmapDrawable) mResultObject).hasValidBitmap() : mResultObject instanceof Bitmap
                ? !((Bitmap) mResultObject).isRecycled() : false);
    }

    public ImageLoadObject setup(MIM mim, Object displayObject) {
        this.mim = mim;
        this.mDisplayObject = displayObject;
        if (width == 0 || height == 0) {
            width = mim.getWidth();
            height = mim.getHeight();
        }
        if (PreLoadResource == -1) {
            PreLoadResource = mim.getPreLoadResource();
        }
        if (BadLoadResource == -1) {
            BadLoadResource = mim.getBadLoadResource();
        }
        if (mMaker == null) {
            mMaker = mim.getMaker();
        }
        if (mDisplayer == null) {
            mDisplayer = mim.getDisplayer();
        }
        if (mPostMaker == null) {
            mPostMaker = mim.getPostMaker();
        }
        if (mAnalyzer == null) {
            mAnalyzer = mim.getAnalyzer();
        }
        if (mCleanPrevious == null) {
            mCleanPrevious = new AtomicBoolean(mim.cleanPreviouse());
        }
        if (mSaveInCache == null) {
            mSaveInCache = new AtomicBoolean(mim.saveInCache());
        }
        if (mSaveInDiscCache == null) {
            mSaveInDiscCache = new AtomicBoolean(mim.saveInDiskCache());
        }
        if (mPostProcess == null) {
            mPostProcess = new AtomicBoolean(mim.postProcess());
        }
        if (mAnalyzeColors == null) {
            mAnalyzeColors = new AtomicBoolean(mim.analyze());
        }
        if (mAnimationEnable == null) {
            mAnimationEnable = new AtomicBoolean(mim.animationEnable());
        }
        if (mCacheAnimationEnable == null) {
            mCacheAnimationEnable = new AtomicBoolean(mim.cacheAnimationEnable());
        }
        if (mScaleToFit == null) {
            mScaleToFit = new AtomicBoolean(mim.scaleToFit());
        }
        if (mUseRecycleDrawable == null) {
            mUseRecycleDrawable = new AtomicBoolean(mim.useRecycleDrawable());
        }
        return this;
    }

    public void cancel() {
        isCanceled = true;

    }

    public static boolean cancel(View displayObject) {
        ImageLoadObject loadObject = getAttachedObject(displayObject, true);
        if (loadObject != null) {
            loadObject.cancel();
            return true;
        }
        return false;
    }

    public static boolean cancel(Object displayObject, String key) {
        boolean canceled = true;
        ImageLoadObject task = getAttachedObject(displayObject, true);
        if (task != null) {
            final String taskKey = task.getKey();
            if (null != taskKey && taskKey.equals(key)) {
                canceled = task.isCanceled() || task.isFinished;
            } else {
                task.cancel();
            }
        }
        return canceled;
    }

    public static ImageLoadObject getAttachedObject(Object object, boolean cancel) {
        if (object != null && object instanceof View) {
            View v = (View) object;
            Object taskObject = v.getTag(R.id.MIM_TASK_TAG);
            if (taskObject != null && taskObject instanceof ImageLoadObject) {
                if (cancel)
                    v.setTag(R.id.MIM_TASK_TAG, null);
                return (ImageLoadObject) taskObject;
            }
        }
        return null;
    }

    public void notifyStart() {
        if (mListener != null) {
            mListener.onImageLoadEvent(OnImageLoadEventListener.IMAGE_LOAD_EVENT.START, this);
        }
    }

    public void notifyFinish() {
        if (this == getAttachedObject(mDisplayObject, false) || isLoadedFromCache) {
            if (mResultObject == null && BadLoadResource != -1) {
                MIMUtils.setImageByViewType(mDisplayObject, BadLoadResource);
            } else if (mResultObject != null) {
                if (mResultObject instanceof Drawable) {
                    MIMUtils.setImageByViewType(mDisplayObject, (Drawable) mResultObject);
                } else if (mResultObject instanceof Bitmap) {
                    MIMUtils.setImageByViewType(mDisplayObject, (Bitmap) mResultObject);
                }
            }
            if (mAnimationEnable.get() && ((isLoadedFromCache && mCacheAnimationEnable.get()) || !isLoadedFromCache)
                    && mDisplayer != null && mDisplayObject instanceof View) {
                mDisplayer.display((View) mDisplayObject);
            }
        }
        if (mListener != null) {
            mListener.onImageLoadEvent(OnImageLoadEventListener.IMAGE_LOAD_EVENT.FINISH, this);
        }
    }

    @Override
    public void run() {
        try {
            android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            if (!isCanceled)
                handle(MSG_POST_START);
            if (!isCanceled) {
                try {
                    process();
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
            if (!isCanceled)
                mim.rest(this);
            if (!isCanceled) {
                handle(MSG_POST_RESULT);
            }
        } finally {
            isFinished = true;
        }
    }

    protected void process() {
        if (mPreLoadListener != null) {
            mPreLoadListener.onPreLoad(this);
        }
        boolean fromDiscCache = false;
        if (mim.getDiskCache() != null) {
            mResultObject = mim.getDiskCache().loadBitmap(path, this);
        }
        if (mResultObject == null) {
            mResultObject = mMaker.getBitmap(this, mim.getContext());
        } else {
            fromDiscCache = true;
        }
        if (mResultObject != null) {
            if (mPostProcess.get() && mPostMaker != null && !fromDiscCache) {
                mResultObject = mPostMaker.processBitmap(this, (Bitmap) mResultObject);
            }
            if (mAnalyzeColors.get() && mAnalyzer != null) {
                mAnalyzedColors = mAnalyzer.analyze((Bitmap) mResultObject);
            }
            if (mSaveInDiscCache.get() && mim.getDiskCache() != null && !fromDiscCache) {
                mim.getDiskCache().asyncSave(path, (Bitmap) mResultObject);
            }
            if (mUseRecycleDrawable.get() && mResultObject instanceof Bitmap) {
                mResultObject = makeRecycleDrawable(mim.getContext().getResources(), (Bitmap) mResultObject);
            }
            if (mResultObject != null) {
                if (mSaveInCache.get() && mim.getCache() != null) {
                    mim.getCache().put(key, this);
                }
            }
        }
    }

    private void handle(int msg) {
        uiHandler.obtainMessage(msg, this).sendToTarget();
    }

    private boolean fromAtomic(AtomicBoolean atomicBoolean) {
        return atomicBoolean != null && atomicBoolean.get();
    }

    public ImageLoadObject async() {
        if (!prepare())
            mim.getExecutor().execute(this);
        return this;
    }

    public ImageLoadObject block() {
        if (!prepare())
            process();
        return this;
    }

    public boolean prepare() {
        if (mim.getCache() != null) {
            ImageLoadObject loadObject = (ImageLoadObject) mim.getCache().get(key);
            if (loadObject != null) {
                if (isFresh) {
                    mim.getCache().remove(loadObject.getKey());
                } else {
                    if (mDisplayObject instanceof View)
                        cancel((View) mDisplayObject);
                    isLoadedFromCache = true;
                    setResult(loadObject);
                    notifyFinish();
                    return true;
                }
            }
        }

        if (cancel(mDisplayObject, key)) {
            isFinished = false;
            if (mDisplayObject instanceof View) {
                View displayView = (View) mDisplayObject;
                displayView.setTag(R.id.MIM_TASK_TAG, this);
                if (mCleanPrevious.get()) {
                    if (PreLoadResource != -1) {
                        MIMUtils.setImageByViewType(mDisplayObject, PreLoadResource);
                    } else {
                        MIMUtils.setImageByViewType(mDisplayObject, new ColorDrawable());
                    }
                }
            }
            return false;
        }

        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o instanceof ImageLoadObject) {
            ImageLoadObject object = (ImageLoadObject) o;
            return object.path != null && path != null ? object.path.equals(path) : (object.key != null && key != null
                    ? object.key.equals(key) : object.key == null && key == null);
        }
        return false;
    }

    @Override
    public String toString() {
        return path != null ? String.valueOf(path.hashCode()) : key;
    }

    public ImageLoadObject clone() {
        try {
            return (ImageLoadObject) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public interface OnImageLoadEventListener {

        void onImageLoadEvent(IMAGE_LOAD_EVENT event, ImageLoadObject loadObject);

        enum IMAGE_LOAD_EVENT {
            START, FINISH
        }
    }

    public static abstract class SimpleListener implements OnImageLoadEventListener {

        @Override
        public void onImageLoadEvent(IMAGE_LOAD_EVENT event, ImageLoadObject loadObject) {
            switch (event) {
                case FINISH:
                    if (loadObject.getResultObject() != null) {
                        onSuccess(loadObject);
                    } else {
                        onFail();
                    }
                    break;
            }
        }

        public abstract void onSuccess(ImageLoadObject loadObject);

        public abstract void onFail();

    }

}
