package com.dream.android.mim;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Artyom on 3/9/2015.
 */
public class MIMColorAnalyzer {

    public static final String TAG = MIMColorAnalyzer.class.getSimpleName();

    static {
        try {
            System.loadLibrary("MIMNative-v1");
        } catch (UnsatisfiedLinkError e) {
        }
    }

    private int accuracy = 3;
    private double lightness = 0.5;

    public MIMColorAnalyzer() {
    }

    public MIMColorAnalyzer(int accuracy, double lightness){
        this.accuracy = accuracy;
        this.lightness = lightness;
    }

    public MIMColorAnalyzer setAccuracy(int accuracy) {
        this.accuracy = accuracy;
        return this;
    }

    public MIMColorAnalyzer setLigtness(double lightness) {
        this.lightness = lightness;
        return this;
    }

    public Integer[] analyze(Bitmap bitmap) {
        try {
            if (bitmap != null) {
                int width = bitmap.getWidth();
                int height = bitmap.getHeight();


                final HashMap<Integer, AtomicInteger> colors = new HashMap<>();

                for (int y = 0; y < height; y += accuracy) {
                    for (int x = 0; x < width; x += accuracy) {
                        int color = bitmap.getPixel(x, y);
                        if (MIMUtils.getColorLightness(color) < lightness)
                            if (!colors.containsKey(color)) {
                                colors.put(color, new AtomicInteger(1));
                            } else {
                                colors.get(color).incrementAndGet();
                            }
                    }
                }

                ArrayList<Integer> keys = new ArrayList<>();
                keys.addAll(colors.keySet());

                Collections.sort(keys, new Comparator<Integer>() {
                    @Override
                    public int compare(Integer lhs, Integer rhs) {
                        AtomicInteger val1 = colors.get(lhs);
                        AtomicInteger val2 = colors.get(rhs);
                        if (val1 == null) {
                            return (val2 != null) ? 1 : 0;
                        } else if (val1 != null && val2 != null) {
                            return val1.get() - val2.get();
                        } else {
                            return 0;
                        }
                    }
                });
//                    Log.e(TAG, "simple analyze time = " + (System.currentTimeMillis() - startTime) + ", size = " + keys.size());
//                    startTime = System.currentTimeMillis();
//                    int[] nativeAnalyzed = analyze(bitmap, accuracy, lightness);
//                    Log.e(TAG, "native analyze time = " + (System.currentTimeMillis() - startTime) + ", size = " + nativeAnalyzed.length);

                return keys.toArray(new Integer[keys.size()]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public native int[] analyze(Bitmap bitmap, int accuracy, double lightness);

}
