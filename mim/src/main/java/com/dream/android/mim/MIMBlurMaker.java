package com.dream.android.mim;

import android.graphics.Bitmap;

/**
 * Created by Artyom on 1/21/2015.
 */
public class MIMBlurMaker implements MIMAbstractPostMaker {

    static {
        System.loadLibrary("MIMNative-v1");
    }

    private int mBlur = 15;

    public MIMBlurMaker() {
    }

    public MIMBlurMaker(int blur) {
        mBlur = blur;
    }

    @Override
    public Bitmap processBitmap(ImageLoadObject object, Bitmap bitmap) {
        blur(bitmap, mBlur);
        return bitmap;
    }

    public native void blur(Bitmap bitmap, int radius);

}
