package com.dream.android.mim;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.util.*;

public class MIMDiskCache {

    public static final String TAG = MIMDiskCache.class.getSimpleName();

    private static final Object mLock = new Object();

    private static volatile MIMDiskCache instance;
    private File cacheFile;
    private long busySpace;
    private long MAX_SPACE = 50 * 1000 * 1024;
    private SaveTask mCurrentTask = null;
    private NavigableMap<Object, Bitmap> mSaveStack = new TreeMap<>();
    private Comparator<File> dateCompare = new Comparator<File>() {
        @Override
        public int compare(File lhs, File rhs) {
            return (int) (lhs.lastModified() - rhs.lastModified());
        }
    };

    public MIMDiskCache() {
    }

    public static MIMDiskCache getInstance() {
        MIMDiskCache localInstance = instance;
        if (localInstance == null) {
            synchronized (MIMDiskCache.class) {
                localInstance = instance;
                if (localInstance == null)
                    localInstance = instance = new MIMDiskCache();
            }
        }
        return localInstance;
    }

    public static String makeCacheDirPath(String cacheDir) {
        return cacheDir + File.separator + "MIM_CACHE";
    }

    public boolean isInitialized() {
        return cacheFile != null;
    }

    public MIMDiskCache init(File file, int maxSize) {
        MAX_SPACE = maxSize;
        return init(file);
    }

    public MIMDiskCache init(File file) {
        return init(file.getPath());
    }

    public MIMDiskCache init(String cacheDir) {
        if (cacheFile != null && cacheFile.getPath().equals(cacheDir))
            return this;
        assert cacheDir != null;
        cacheFile = new File(makeCacheDirPath(cacheDir));
        if (!cacheFile.exists())
            cacheFile.mkdirs();
        File[] files = cacheFile.listFiles();
        if (files != null) {
            for (File _f : files)
                busySpace += _f.length();
        }
        Log.e(TAG, "busySpace: " + busySpace);
        return this;
    }

    public boolean saveBitmap(String source, Bitmap bitmap) {
        source = MIMUtils.makePathFromUrl(source);
        assert cacheFile != null : "Cache file not initialized!";
        assert bitmap != null : "bitmap is null";
        long size = bitmap.getHeight() * bitmap.getRowBytes();
        if (!isHaveSpace(size)) {
            clearOld(size);
        }
        try {
            if (bitmap.isRecycled()) {
                return false;
            }
            Log.e(TAG, "bitmap size: " + bitmap.getByteCount());
            File f = new File(cacheFile, source + ".png");
            FileOutputStream stream = new FileOutputStream(f);
            bitmap.compress(CompressFormat.PNG, 100, stream);
            stream.close();
            Log.e(TAG, "bitmap size: " + bitmap.getByteCount() + ", file size: " + f.length());
            busySpace += f.length();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Bitmap loadBitmap(Object source, ImageLoadObject loadObject) {
//        synchronized (mLock) {
            String saveKey = MIMUtils.makePathFromUrl(source.toString());
            if (saveKey == null) {
                return null;
            }
            File f = new File(cacheFile, saveKey + ".png");
            if (f.exists()) {
                BitmapFactory.Options options = null;
                if (loadObject != null) {
                    options = new BitmapFactory.Options();
                    options.inPreferredConfig = loadObject.getConfig();
                }
                return BitmapFactory.decodeFile(f.getPath(), options);
            }
            return null;
//        }
    }

    private boolean isHaveSpace(long size) {
        return MAX_SPACE > busySpace + size;
    }

    private void clearOld(long neededSize) {
        synchronized (mLock) {
            File[] files = cacheFile.listFiles();
            Arrays.sort(files, dateCompare);
            for (File _f : files) {
                long _s = _f.length();
                if (_f.delete()) {
                    busySpace -= _s;
                    if (busySpace + neededSize < MAX_SPACE) {
                        break;
                    }
                }
            }
        }
    }

    public void asyncSave(Object key, Bitmap image) {
        if (mSaveStack.containsKey(key))
            return;
        SaveTask task = new SaveTask();
        task.execute(key, image);
    }

    private void processAsyncSave() {
        synchronized (mSaveStack) {
            if (mSaveStack.size() > 0) {
                Map.Entry<Object, Bitmap> entry = mSaveStack.lastEntry();
                mCurrentTask = new SaveTask();
                mCurrentTask.execute(entry.getKey(), entry.getValue());
            }
        }
    }

    private class SaveTask extends AsyncTask<Object, String, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Object... objects) {
            boolean saved = false;
            try {
                saved = saveBitmap(objects[0].toString(), (Bitmap) objects[1]);
                synchronized (mSaveStack) {
                    mSaveStack.remove(objects[0]);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return saved;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            mCurrentTask = null;
            processAsyncSave();
        }
    }
}
