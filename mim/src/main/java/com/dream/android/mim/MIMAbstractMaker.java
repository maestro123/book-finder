package com.dream.android.mim;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;


public abstract class MIMAbstractMaker {

    public abstract Bitmap getBitmap(ImageLoadObject loadObject, Context context);

}
