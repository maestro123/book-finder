package com.dream.android.mim;

import android.content.Context;
import android.graphics.*;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ViewTreeObserver;

/**
 * Created by Artyom on 3/9/2015.
 */
public class MSlideImageView extends RecyclingImageView implements ViewTreeObserver.OnScrollChangedListener {

    private Paint paint = new Paint();
    private boolean useWindowAdjust = true;
    private float topPercent = 0.5f;
    private float multiplier = 1.4f;

    public MSlideImageView(Context context) {
        super(context);
        init();
    }

    public MSlideImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    final void init() {
        applyWindowAdjust();
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
    }

    private void applyWindowAdjust() {
        if (useWindowAdjust)
            getViewTreeObserver().addOnScrollChangedListener(this);
        else
            getViewTreeObserver().removeOnScrollChangedListener(this);
    }

    public void setUseWindowAdjust(boolean value) {
        useWindowAdjust = value;
        applyWindowAdjust();
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        Drawable drawable = getDrawable();
        if (drawable == null || !(drawable instanceof BitmapDrawable)) {
            return;
        }
        if (getWidth() == 0 || getHeight() == 0) {
            return;
        }
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        if (bitmap == null || bitmap.isRecycled())
            return;

        Matrix mMatrix = new Matrix();
        int defWidth = getDrawable().getIntrinsicWidth();
        int defHeight = getDrawable().getIntrinsicHeight();
        float xScale = getWidth() * multiplier / defWidth;
        float yScale = getHeight() * multiplier / defHeight;
        mMatrix.postScale(xScale, yScale);
        mMatrix.postTranslate(-(getWidth() * multiplier - getWidth()) / 2, -(getHeight() * multiplier - getHeight()) * topPercent);

        canvas.drawBitmap(bitmap, mMatrix, paint);
    }

    @Override
    public void onScrollChanged() {
        Rect mRect = new Rect();
        getGlobalVisibleRect(mRect);
        float newTopPercent = (float) (mRect.centerY()) / getResources().getDisplayMetrics().heightPixels;
        if (Math.abs(newTopPercent - topPercent) > getResources().getDisplayMetrics().density / 1000) {
            topPercent = newTopPercent;
            invalidate();
        }
    }
}
