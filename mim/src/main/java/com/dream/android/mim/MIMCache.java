package com.dream.android.mim;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by artyom on 2.4.15.
 */
public class MIMCache {

    public static final String TAG = MIMCache.class.getSimpleName();

    private static final int DEF_PART = 10;

    private static volatile MIMCache instance;

    public static synchronized MIMCache getInstance() {
        return instance != null ? instance : (instance = new MIMCache());
    }

    private final ArrayList<Object> mKeys = new ArrayList<>();
    private final ArrayList<Object> mValues = new ArrayList<>();

    private int maxSize;
    private int usedSize;

    public MIMCache() {
        init(partOfRuntime(DEF_PART));
    }

    public MIMCache(int size) {
        init(size);
    }

    public void setSize(int size){
        clear();
        init(size);
    }

    private void init(int size) {
        maxSize = size;
    }

    public static int partOfRuntime(int part) {
        return Math.round(Runtime.getRuntime().maxMemory() / 1024 / part);
    }

    public void put(Object key, Object object) {
        synchronized (mKeys) {
            final int objectSize = sizeOf(object);
            if (usedSize + objectSize > maxSize) {
                Log.e(TAG, "free spacing");
                free(objectSize);
            }
            usedSize += objectSize;
            mKeys.add(key);
            mValues.add(object);
            if (object instanceof RecyclingBitmapDrawable) {
                ((RecyclingBitmapDrawable) object).setIsCached(true);
            } else if (object instanceof ImageLoadObject) {
                Object resultObject = ((ImageLoadObject) object).getResultObject();
                if (resultObject instanceof RecyclingBitmapDrawable) {
                    ((RecyclingBitmapDrawable) resultObject).setIsCached(true);
                }
            }
        }
    }

    public Object get(Object key) {
        synchronized (mKeys) {
            int index = mKeys.indexOf(key);
            return index != -1 ? mValues.get(index) : null;
        }
    }

    public void remove(Object key) {
        synchronized (mKeys) {
            Object object = removeValue(key);
            if (object != null) {
                int objectSize = sizeOf(object);
                usedSize -= objectSize;
                objectRemoved(object);
            }
        }
    }

    public void free(int requiredSize) {
        synchronized (mKeys) {
            final int starSize = usedSize;
            ArrayList<Object> keys = new ArrayList<>(mKeys);
            for (Object key : keys) {
                remove(key);
                if (starSize - usedSize >= requiredSize) {
                    break;
                }
            }
        }
    }

    public void clear() {
        synchronized (mKeys) {
            for (Object value : mValues) {
                objectRemoved(value);
            }
            usedSize = 0;
            mKeys.clear();
            mValues.clear();
        }
    }

    public int sizeOf(Object object) {
        if (object instanceof ImageLoadObject) {
            object = ((ImageLoadObject) object).getResultObject();
        }
        if (object instanceof Bitmap) {
            return ((Bitmap) object).getByteCount() / 1024;
        } else if (object instanceof RecyclingBitmapDrawable) {
            return ((RecyclingBitmapDrawable) object).getBitmap().getByteCount() / 1024;
        } else {
            throw new UnsupportedOperationException("Unknown object for get size");
        }
    }

    public void objectRemoved(Object object) {
        if (object instanceof ImageLoadObject) {
            object = ((ImageLoadObject) object).getResultObject();
        }
        if (object instanceof Bitmap) {
            ((Bitmap) object).recycle();
        } else if (object instanceof RecyclingBitmapDrawable) {
            ((RecyclingBitmapDrawable) object).setIsCached(false);
        } else {
            throw new UnsupportedOperationException("Unknown object for process remove");
        }
    }

    private Object removeValue(Object key) {
        final int index = mKeys.indexOf(key);
        if (index != -1) {
            Object object = mValues.remove(index);
            mKeys.remove(key);
            return object;
        }
        return null;
    }

}