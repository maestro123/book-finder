package com.dream.android.mim;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

/**
 * Created by artyom on 7/23/14.
 */
public class RecyclingImageView extends ImageView {

    public static String TAG = RecyclingImageView.class.getSimpleName();
    protected boolean mBlockRequest = false;
    private boolean hasFixedSize = false;
    private boolean mReleaseOnDetach = true;
    private ImageLoadObject mLoadObject;
    private int mWidth = -1;
    private int mHeight = -1;

    public RecyclingImageView(Context context) {
        super(context);
    }

    public RecyclingImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RecyclingImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public static void notifyDrawable(Drawable drawable, final boolean isDisplayed) {
        if (drawable instanceof RecyclingBitmapDrawable) {
            ((RecyclingBitmapDrawable) drawable).setIsDisplayed(isDisplayed);
        } else if (drawable instanceof LayerDrawable) {
            LayerDrawable layerDrawable = (LayerDrawable) drawable;
            for (int i = 0, z = layerDrawable.getNumberOfLayers(); i < z; i++) {
                notifyDrawable(layerDrawable.getDrawable(i), isDisplayed);
            }
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        notifyDrawable(getDrawable(), false);
        if (mReleaseOnDetach) {
            Object task = getTag(R.id.MIM_TASK_TAG);
            if (task != null && task instanceof ImageLoadObject) {
                ((ImageLoadObject) task).cancel();
            }
        }
        super.onDetachedFromWindow();
    }

    @Override
    public void unscheduleDrawable(Drawable who) {
        super.unscheduleDrawable(who);
        if (who instanceof RecyclingBitmapDrawable) {
            ((RecyclingBitmapDrawable) who).checkState();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!hasValidRecyclingBitmapDrawable())
            notifyDrawable(null, false);
        else
            notifyDrawable(getDrawable(), true);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;
        if (mLoadObject != null && (mWidth > 0 || mHeight > 0)) {
            mLoadObject.size(mWidth, mHeight).async();
            mLoadObject = null;
        }
    }

    public void setLoadObject(ImageLoadObject loadObject) {
        if ((mWidth > 0 || mHeight > 0) && loadObject != null) {
            loadObject.size(mWidth, mHeight).async();
        } else {
            mLoadObject = loadObject;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Drawable drawable = getDrawable();
        if (drawable instanceof BitmapDrawable) {
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            if (bitmap != null && bitmap.isRecycled()) {
                canvas.drawColor(Color.RED);
                return;
            }
        }
        super.onDraw(canvas);
    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        blockRequestIfPossible();
        final Drawable previousDrawable = getDrawable();
        super.setImageDrawable(drawable);
        notifyDrawable(drawable, true);
        notifyDrawable(previousDrawable, false);
        mBlockRequest = false;
    }

    @Override
    public void requestLayout() {
        if (!mBlockRequest) {
            super.requestLayout();
        }
    }

    protected void blockRequestIfPossible() {
        if (hasFixedSize) {
            mBlockRequest = true;
        }
    }

    public void setReleaseOnDetach(boolean release) {
        mReleaseOnDetach = release;
    }

    public void setHasFixedSize(boolean value) {
        hasFixedSize = value;
    }

    public boolean hasValidRecyclingBitmapDrawable() {
        Drawable drawable = getDrawable();
        return drawable instanceof RecyclingBitmapDrawable
                ? ((RecyclingBitmapDrawable) drawable).hasValidBitmap() : false;
    }

}
