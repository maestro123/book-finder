package com.dream.android.mim;

import android.graphics.Bitmap;

/**
 * Created by artyom on 6/13/14.
 */
public interface MIMAbstractPostMaker {

    Bitmap processBitmap(ImageLoadObject object, Bitmap bitmap);

}
