package com.dream.android.mim;

import android.content.Context;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * Created by artyom on 2.4.15.
 */
public class MIM {

    public static final String TAG = MIM.class.getSimpleName();

    private final Object mPauseWorkLock = new Object();
    private boolean mPauseWork = false;

    private Context mContext;
    private ExecutorService mExecutor;
    private MIMCache mCache;
    private MIMDiskCache mDiskCache;

    private int width;
    private int height;

    private int PreLoadResource = -1;
    private int BadLoadResource = -1;

    private MIMAbstractMaker mMaker = new MIMDefaultMaker();
    private MIMAbstractDisplayer mDisplayer = new MIMAlphaDisplayer(300);
    private MIMAbstractPostMaker mPostMaker = null;
    private MIMColorAnalyzer mAnalyzer = new MIMColorAnalyzer();

    private boolean mCleanPrevious = true;
    private boolean mSaveInCache = true;
    private boolean mSaveInDiscCache = true;
    private boolean mPostProcess = true;
    private boolean mAnalyzeColors = false;
    private boolean mAnimationEnable = true;
    private boolean mCacheAnimationEnable = false;
    private boolean mScaleToFit = false;
    private boolean mUseRecycleDrawable = true;

    public MIM(Context context) {
        mContext = context;
        mCache = MIMCache.getInstance();
        setThreadCount(Runtime.getRuntime().availableProcessors() + 1);
    }

    public MIM setWidth(int width) {
        this.width = width;
        return this;
    }

    public int getWidth() {
        return width;
    }

    public MIM setHeight(int height) {
        this.height = height;
        return this;
    }

    public int getHeight() {
        return height;
    }

    public MIM size(int width, int height) {
        this.width = width;
        this.height = height;
        return this;
    }

    public MIM preLoadResource(int resource) {
        PreLoadResource = resource;
        return this;
    }

    public int getPreLoadResource() {
        return PreLoadResource;
    }

    public MIM badLoadResource(int resource) {
        BadLoadResource = resource;
        return this;
    }

    public int getBadLoadResource() {
        return BadLoadResource;
    }

    public MIM maker(MIMAbstractMaker maker) {
        mMaker = maker;
        return this;
    }

    public MIMAbstractMaker getMaker() {
        return mMaker;
    }

    public MIM displayer(MIMAbstractDisplayer displayer) {
        mDisplayer = displayer;
        return this;
    }

    public MIMAbstractDisplayer getDisplayer() {
        return mDisplayer;
    }

    public MIM postMaker(MIMAbstractPostMaker postMaker) {
        mPostMaker = postMaker;
        return this;
    }

    public MIMAbstractPostMaker getPostMaker() {
        return mPostMaker;
    }

    public MIM analyzer(MIMColorAnalyzer analyzer) {
        mAnalyzer = analyzer;
        return this;
    }

    public MIMColorAnalyzer getAnalyzer() {
        return mAnalyzer;
    }

    public MIM cleanPreviouse(boolean value) {
        mCleanPrevious = value;
        return this;
    }

    public boolean cleanPreviouse() {
        return mCleanPrevious;
    }

    public MIM cache(boolean value) {
        mSaveInCache = value;
        return this;
    }

    public boolean saveInCache() {
        return mSaveInCache;
    }

    public MIM diskCache(boolean value) {
        mSaveInDiscCache = value;
        return this;
    }

    public boolean saveInDiskCache() {
        return mSaveInDiscCache;
    }

    public MIM postProcess(boolean value) {
        mPostProcess = value;
        return this;
    }

    public boolean postProcess() {
        return mPostProcess;
    }

    public MIM analyze(boolean value) {
        mAnalyzeColors = value;
        return this;
    }

    public boolean analyze() {
        return mAnalyzeColors;
    }

    public MIM animationEnable(boolean value) {
        mAnimationEnable = value;
        return this;
    }

    public boolean animationEnable() {
        return mAnimationEnable;
    }

    public MIM cacheAnimationEnable(boolean value) {
        mCacheAnimationEnable = value;
        return this;
    }

    public boolean cacheAnimationEnable() {
        return mCacheAnimationEnable;
    }

    public MIM scaleToFit(boolean value) {
        mScaleToFit = value;
        return this;
    }

    public boolean scaleToFit() {
        return mScaleToFit;
    }

    public MIM useRecycleDrawable(boolean value) {
        mUseRecycleDrawable = value;
        return this;
    }

    public boolean useRecycleDrawable() {
        return mUseRecycleDrawable;
    }

    public MIM setThreadCount(int threadCount) {
        if (mExecutor != null) {
            mExecutor.shutdownNow();
        }
        mExecutor = Executors.newFixedThreadPool(threadCount);
        return this;
    }

    public MIM setCache(MIMCache cache) {
        mCache = cache;
        return this;
    }

    public MIM setDiskCache(MIMDiskCache cache) {
        mDiskCache = cache;
        return this;
    }

    public Context getContext() {
        return mContext;
    }

    public ExecutorService getExecutor() {
        return mExecutor;
    }

    public MIMCache getCache() {
        return mCache;
    }

    public MIMDiskCache getDiskCache() {
        return mDiskCache;
    }

    protected final void rest(ImageLoadObject loadObject) {
        synchronized (mPauseWorkLock) {
            while (mPauseWork && !loadObject.isCanceled()) {
                try {
                    mPauseWorkLock.wait();
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public void pause() {
        synchronized (mPauseWorkLock) {
            mPauseWork = true;
        }
    }

    public void resume() {
        synchronized (mPauseWorkLock) {
            mPauseWork = false;
            mPauseWorkLock.notifyAll();
        }
    }

    public static MIM by(String mimKey) {
        return MIMManager.getInstance().getMIM(mimKey);
    }

    public ImageLoadObject of(ImageLoadObject loadObject) {
        return loadObject.setup(this, null);
    }

    public ImageLoadObject of(String path) {
        return new ImageLoadObject(path).setup(this, null);
    }

    public ImageLoadObject of(String key, String path) {
        return new ImageLoadObject(key, path).setup(this, null);
    }

    public ImageLoadObject to(Object viewObject, String path) {
        return new ImageLoadObject(path).setup(this, viewObject);
    }

    public ImageLoadObject to(Object viewObject, String key, String path) {
        return new ImageLoadObject(key, path).setup(this, viewObject);
    }

    public ImageLoadObject to(Object viewObject, ImageLoadObject loadObject) {
        return loadObject.setup(this, viewObject);
    }

}