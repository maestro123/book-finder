package com.dream.android.mim;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class MIMInternetMaker extends MIMDefaultMaker {

    public static final String TAG = MIMInternetMaker.class.getSimpleName();

    @Override
    public Bitmap getBitmap(ImageLoadObject loadObject, Context ctx) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = loadObject.getConfig();
            Bitmap bitmap = BitmapFactory.decodeStream(getNetworkStream(loadObject.getPath()), null, options);
            if (loadObject.scaleToFit()) {
                Bitmap scaledBitmap = Bitmap.createBitmap(loadObject.getWidth(), loadObject.getHeight(), Bitmap.Config.ARGB_8888);

                float ratioX = loadObject.getWidth() / (float) bitmap.getWidth();
                float ratioY = loadObject.getHeight() / (float) bitmap.getHeight();
                float middleX = loadObject.getWidth() / 2.0f;
                float middleY = loadObject.getHeight() / 2.0f;

                Matrix scaleMatrix = new Matrix();
                scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
                Canvas canvas = new Canvas(scaledBitmap);
                canvas.setMatrix(scaleMatrix);
                Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
                paint.setAntiAlias(true);

                canvas.drawBitmap(bitmap, middleX - bitmap.getWidth() / 2, middleY - bitmap.getHeight() / 2, paint);
                bitmap.recycle();
                return scaledBitmap;
            } else {
                return bitmap;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public InputStream getNetworkStream(String path) throws IOException {
        return getUrlStream(new URL(path));
    }

    private InputStream getUrlStream(URL url) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setConnectTimeout(120000);
        conn.setReadTimeout(120000);
        return conn.getInputStream();
    }

}
