package com.dream.android.mim;

import android.graphics.*;

/**
 * Created by artyom on 8/14/14.
 */
public class MIMRoundRectPostMaker implements MIMAbstractPostMaker {

    public static final float HALF_RADIUS = -2;

    private float mRadius = 0;

    private Paint mPaint = new Paint();

    {
        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.TRANSPARENT);
    }

    private Paint mRectPaint = null;
    private float mRectRadius;


    public MIMRoundRectPostMaker(float radius) {
        mRadius = radius;
    }

    public MIMRoundRectPostMaker(float radius, int color, int rectRadius, int width) {
        this(radius);
        mRectPaint = new Paint();
        mRectPaint.setAntiAlias(true);
        mRectPaint.setStrokeWidth(width);
        mRectPaint.setStyle(Paint.Style.STROKE);
        mRectPaint.setColor(color);
        mRectRadius = rectRadius;
    }


    @Override
    public Bitmap processBitmap(ImageLoadObject object, Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, mRadius, mRadius, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        if (mRectPaint != null) {
            final float padding = mRectPaint.getStrokeWidth() / 2;
            rectF.left += padding;
            rectF.top += padding;
            rectF.right -= padding;
            rectF.bottom -= padding;
            canvas.drawRoundRect(rectF, mRadius - mRadius / 2, mRadius - mRadius / 2, mRectPaint);
        }
        bitmap.recycle();
        return output;
    }
}
