LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := MIMNative-v1
LOCAL_CFLAGS := -fexceptions
LOCAL_LDFLAGS := -Wl,--build-id
LOCAL_LDLIBS := \
	-llog \
	-ljnigraphics \

LOCAL_SRC_FILES := \
	C:\Projects\book-finder\mim\src\main\jni\Android.mk \
	C:\Projects\book-finder\mim\src\main\jni\Application.mk \
	C:\Projects\book-finder\mim\src\main\jni\MIMNative\CMakeLists.txt \
	C:\Projects\book-finder\mim\src\main\jni\MIMNative\MIMNative.cpp \

LOCAL_C_INCLUDES += C:\Projects\book-finder\mim\src\main\jni
LOCAL_C_INCLUDES += C:\Projects\book-finder\mim\src\release\jni

include $(BUILD_SHARED_LIBRARY)
