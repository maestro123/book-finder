package maestro.bookfinder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.astuetz.PagerSlidingTabStrip;

import maestro.bookfinder.model.Catalog;
import maestro.bookfinder.model.CatalogItem;
import maestro.bookfinder.model.CatalogPage;

/**
 * Created by Artyom.Borovsky on 29.09.2015.
 */
public class CatalogFragment extends Fragment implements LoaderManager.LoaderCallbacks<CatalogItem> {

    public static final String TAG = CatalogFragment.class.getSimpleName();

    public static final CatalogFragment makeInstance(long id) {
        CatalogFragment fragment = new CatalogFragment();
        Bundle args = new Bundle(1);
        args.putLong(PARAM_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    private static final String PARAM_ID = "id";

    private ViewPager mPager;
    private PagerSlidingTabStrip mIndicator;
    private PageAdapter mAdapter;
    private Catalog mCatalog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.catalog_fragment_view, null);
        mPager = (ViewPager) v.findViewById(R.id.pager);
        mIndicator = (PagerSlidingTabStrip) v.findViewById(R.id.tab_indicator);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mCatalog = ((BookFinderApplication) getActivity().getApplication())
                .getCatalog(getArguments() != null ? getArguments().getLong(PARAM_ID) : 0);
        mPager.setAdapter(mAdapter = new PageAdapter(getChildFragmentManager()));
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public Loader<CatalogItem> onCreateLoader(int id, Bundle args) {
        return new CatalogLoader(getContext(), mCatalog, null);
    }

    @Override
    public void onLoadFinished(Loader<CatalogItem> loader, CatalogItem data) {
        if (data instanceof CatalogPage) {
            mAdapter.update((CatalogPage) data);
            mIndicator.setViewPager(mPager);
        }
    }

    @Override
    public void onLoaderReset(Loader<CatalogItem> loader) {

    }

    public Catalog getCatalog() {
        return mCatalog;
    }

    public static final class PageAdapter extends FragmentStatePagerAdapter {

        private CatalogPage page;

        public PageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0 && page.haveFilterLinks()) {
                return FiltersFragment.makeInstance(page.getFilterLinks());
            }
            position--;
            return BooksListFragment.makeInstance(page.getList(position));
        }

        @Override
        public int getCount() {
            if (page == null) {
                return 0;
            }
            int count = page.getListCount();
            if (page.haveFilterLinks()) {
                count++;
            }
            return count;
        }

        public void update(CatalogPage page) {
            this.page = page;
            notifyDataSetChanged();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (page.haveFilterLinks() && position == 0) {
                return "Filters";
            }
            position--;
            return page.getList(position).getTitle();
        }

        @Override
        public float getPageWidth(int position) {
            return position == 0 && page != null && page.haveFilterLinks() ? 0.7f : 1f;
        }
    }

    public static final class CatalogLoader extends AsyncTaskLoader<CatalogItem> {

        private Object loadObject;
        private Catalog catalog;

        public CatalogLoader(Context context, Catalog catalog, Object loadObject) {
            super(context);
            this.loadObject = loadObject;
            this.catalog = catalog;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public CatalogItem loadInBackground() {
            return catalog.parse(catalog.getHomeLink());
        }

    }

    public static final class DummyFragment extends Fragment {

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            return new LinearLayout(getContext());
        }

    }

}
