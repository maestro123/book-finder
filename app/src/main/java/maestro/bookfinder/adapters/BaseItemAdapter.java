package maestro.bookfinder.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import maestro.bookfinder.R;

/**
 * Created by Artyom on 9/30/2015.
 */
public abstract class BaseItemAdapter<ItemType> extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements View.OnClickListener, View.OnLongClickListener {

    public static final String TAG = BaseItemAdapter.class.getSimpleName();

    public static final int TYPE_BOTTOM_PROGRESS = 1;

    private Context context;
    private LayoutInflater inflater;
    private List<ItemType> items;
    private OnItemClickListener mClickListener;
    private OnItemLongClickListener mLongClickListener;
    private boolean isShowFooterProgress;

    public interface OnItemClickListener<ItemType> {
        void onItemClick(ItemType item);
    }

    public interface OnItemLongClickListener<ItemType> {
        boolean onItemLongClick(ItemType item);
    }

    public BaseItemAdapter(Context context) {
        this(context, null);
    }

    public BaseItemAdapter(Context context, List<ItemType> items) {
        this.context = context;
        this.items = items;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_BOTTOM_PROGRESS) {
            return new FooterProgressHolder(inflate(R.layout.row_progress));
        }
        RecyclerView.ViewHolder holder = onCreateHolder(viewType);
        holder.itemView.setOnClickListener(this);
        holder.itemView.setOnLongClickListener(this);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final int type = getItemViewType(position);
        if (type != TYPE_BOTTOM_PROGRESS) {
            ((BaseTypeHolder) holder).onBind(getItem(position));
        }
    }

    public Context getContext() {
        return context;
    }

    public LayoutInflater getInflater() {
        return inflater;
    }

    public ItemType getItem(int i) {
        return items.get(i);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mClickListener = listener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener listener) {
        mLongClickListener = listener;
    }

    @Override
    public int getItemCount() {
        int count = items != null ? items.size() : 0;
        if (isShowFooterProgress) {
            count++;
        }
        return count;
    }

    @Override
    public int getItemViewType(int position) {
        if (isShowFooterProgress && position == getItemCount() - 1) {
            return TYPE_BOTTOM_PROGRESS;
        }
        return super.getItemViewType(position);
    }

    public void update(List<ItemType> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    protected View inflate(int resId) {
        return inflater.inflate(resId, null);
    }

    public void setShowFooterProgress(boolean show) {
        Log.e(TAG, "isShowFooterProgress: " + show + ", isShowing: " + isShowFooterProgress + ", getCount: " + getItemCount() + ", items: " + (items != null ? items.size() : 0));
        if (show != isShowFooterProgress) {
            isShowFooterProgress = show;
            notifyDataSetChanged();
        }
    }

    public int getSpanCount(int position, int fullCount) {
        int itemType = getItemViewType(position);
        if (itemType == TYPE_BOTTOM_PROGRESS) {
            return fullCount;
        }
        return 1;
    }

    @Override
    public void onClick(View v) {
        if (mClickListener != null) {
            Object tag = v.getTag();
            if (tag instanceof RecyclerView.ViewHolder) {
                mClickListener.onItemClick(getItem(((RecyclerView.ViewHolder) tag).getAdapterPosition()));
            }
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (mLongClickListener != null) {
            Object tag = v.getTag();
            if (tag instanceof RecyclerView.ViewHolder) {
                return mLongClickListener.onItemLongClick(getItem(((RecyclerView.ViewHolder) tag).getAdapterPosition()));
            }
        }
        return false;
    }

    public abstract RecyclerView.ViewHolder onCreateHolder(int viewType);

    public static abstract class BaseTypeHolder<ItemType> extends RecyclerView.ViewHolder {

        public BaseTypeHolder(View itemView) {
            super(itemView);
            itemView.setTag(this);
        }

        public abstract void onBind(ItemType item);

    }

    public static final class FooterProgressHolder extends BaseTypeHolder {

        public FooterProgressHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void onBind(Object item) {
        }

    }

}
