package maestro.bookfinder.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import maestro.bookfinder.BookFinderApplication;
import maestro.bookfinder.R;
import maestro.bookfinder.model.Catalog;

/**
 * Created by Artyom on 9/30/2015.
 */
public class NavigationAdapter extends BaseItemAdapter<Catalog> {

    public NavigationAdapter(Context context, List<Catalog> items) {
        super(context, items);
    }

    @Override
    public NavigationItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NavigationItemHolder(inflate(R.layout.navigation_item_view));
    }

    @Override
    public NavigationItemHolder onCreateHolder(int viewType) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).getId();
    }

    public class NavigationItemHolder extends BaseTypeHolder<Catalog> {

        ImageView Image;
        TextView Title;
        TextView Language;

        NavigationItemHolder(View v) {
            super(v);
            Image = (ImageView) v.findViewById(R.id.image);
            Title = (TextView) v.findViewById(R.id.title);
            Language = (TextView) v.findViewById(R.id.language);
        }

        @Override
        public void onBind(Catalog catalog) {
            Title.setText(catalog.getTitle());
            if (TextUtils.isEmpty(catalog.getLanguage())) {
                Language.setVisibility(View.GONE);
            } else {
                Language.setText(catalog.getLanguage());
                Language.setVisibility(View.VISIBLE);
            }
            if (!TextUtils.isEmpty(catalog.getImageUrl())){
                Picasso.with(getContext()).load(catalog.getImageUrl()).into(Image);
            }else {
                Picasso.with(getContext()).cancelRequest(Image);
            }
        }

    }

}
