package maestro.bookfinder.adapters;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import maestro.bookfinder.R;
import maestro.bookfinder.model.CatalogEntry;
import maestro.bookfinder.utils.CoverTransformation;

/**
 * Created by Artyom.Borovsky on 06.10.2015.
 */
public class CatalogEntryAdapter extends BaseItemAdapter<CatalogEntry> {

    public static final String TAG = CatalogEntryAdapter.class.getSimpleName();

    public enum Type {
        ROW, GRID
    }

    private CoverTransformation mCoverTransformation;
    private Type mType = Type.GRID;
    private int imageWidth;
    private int imageHeight;
    private int mSpanCount;

    public CatalogEntryAdapter(Context context, List<CatalogEntry> items, int spanCount) {
        super(context, items);

        final int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getContext().getResources().getDisplayMetrics());
        imageWidth = (context.getResources().getDisplayMetrics().widthPixels - padding * spanCount - padding) / spanCount;
        imageHeight = (int) (imageWidth * 1.5f);
        mCoverTransformation = new CoverTransformation(context);
    }

    @Override
    public Holder onCreateHolder(int viewType) {
        return new Holder(inflate(mType == Type.ROW ? R.layout.book_item_view_row : R.layout.book_item_view_grid));
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId().hashCode();
    }

    public void setType(Type type) {
        mType = type;
        notifyDataSetChanged();
    }

    public class Holder extends BaseTypeHolder<CatalogEntry> {

        ImageView Image;
        TextView Title;
        TextView Author;
        TextView Price;

        public Holder(View v) {
            super(v);
            Image = (ImageView) v.findViewById(R.id.image);
            Title = (TextView) v.findViewById(R.id.title);
            Author = (TextView) v.findViewById(R.id.author);
            Price = (TextView) v.findViewById(R.id.price);
            Image.getLayoutParams().width = imageWidth;
            Image.getLayoutParams().height = imageHeight;
        }

        @Override
        public void onBind(CatalogEntry entry) {
            Title.setText(entry.getTitle());
            Author.setText(entry.getAuthors());
            if (entry.getBuyInfo() != null) {
                Price.setTextColor(Color.RED);
                Price.setText(String.format("%s %s", entry.getBuyInfo().getPrice(), entry.getBuyInfo().getCurrency()));
                if (entry.getDownloadLinkCount() > 0) {
                    Log.e(TAG, "display free samples");
                }
            } else {
                Price.setTextColor(Color.GREEN);
                Price.setText("Free");
            }

            if (!TextUtils.isEmpty(entry.getImage())) {
                Picasso.with(getContext()).load(entry.getImage()).fit().transform(mCoverTransformation).into(Image);
            } else {
                Picasso.with(getContext()).cancelRequest(Image);
            }
        }

    }

}
