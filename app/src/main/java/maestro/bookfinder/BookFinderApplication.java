package maestro.bookfinder;

import android.app.Application;
import android.util.Log;

import com.dream.android.mim.MIM;
import com.dream.android.mim.MIMDiskCache;
import com.dream.android.mim.MIMInternetMaker;
import com.dream.android.mim.MIMManager;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;

import maestro.bookfinder.model.Catalog;

/**
 * Created by Artyom.Borovsky on 29.09.2015.
 */
public class BookFinderApplication extends Application {

    public static final String TAG = BookFinderApplication.class.getSimpleName();

    public static final String NETWORK = "network";

    private static final String CATALOG = "catalog";
    private static final String URL = "url";
    private static final String TITLE = "title";
    private static final String ICON = "icon";
    private static final String LANGUAGE = "language";

    private Catalog[] catalogs = new Catalog[0];
    private StringBuilder mBuilder = new StringBuilder();

    @Override
    public void onCreate() {
        super.onCreate();
        prepareCatalogs();
        MIMManager.getInstance().addMIM(NETWORK, new MIM(this).maker(new MIMInternetMaker()).setThreadCount(2));
//                .setDiskCache(MIMDiskCache.getInstance().init(getCacheDir())));
    }

    private void prepareCatalogs() {
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = factory.newPullParser();
            parser.setInput(getAssets().open("catalogs.xml"), null);
            int nextType;
            Catalog catalog = null;
            while ((nextType = parser.next()) != XmlPullParser.END_DOCUMENT) {
                if (nextType == XmlPullParser.START_TAG) {
                    mBuilder.setLength(0);
                    if (Catalog.is(CATALOG, parser)) {
                        catalog = new Catalog();
                    }
                } else if (nextType == XmlPullParser.END_TAG) {
                    if (Catalog.is(CATALOG, parser)) {
                        catalog.setId(catalog.getHomeLink().hashCode());
                        addCatalog(catalog);
                        catalog = null;
                    } else if (Catalog.is(URL, parser)) {
                        catalog.setHomeLink(obtainText());
                    } else if (Catalog.is(TITLE, parser)) {
                        catalog.setTitle(obtainText());
                    } else if (Catalog.is(ICON, parser)) {
                        catalog.setImageUrl(obtainText());
                    } else if (Catalog.is(LANGUAGE, parser)) {
                        catalog.setLanguageCode(obtainText());
                    }
                    mBuilder.setLength(0);
                } else if (nextType == XmlPullParser.TEXT) {
                    mBuilder.append(parser.getText());
                }
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Catalog catalog : catalogs) {
            Log.e(TAG, catalog.toString());
        }
    }

    private String obtainText() {
        return mBuilder.toString();
    }

    private void addCatalog(Catalog catalog) {
        if (catalog != null) {
            Catalog[] dummy = new Catalog[catalogs.length + 1];
            System.arraycopy(catalogs, 0, dummy, 0, catalogs.length);
            catalogs = dummy;
            catalogs[catalogs.length - 1] = catalog;
        }
    }

    public Catalog[] getCatalogs() {
        synchronized (catalogs) {
            return catalogs;
        }
    }

    public Catalog getCatalog(long id) {
        synchronized (catalogs) {
            for (Catalog catalog : catalogs) {
                if (catalog.getId() == id) {
                    return catalog;
                }
            }
            return null;
        }
    }

}
