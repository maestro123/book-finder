package maestro.bookfinder;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.RecyclerView;

import maestro.bookfinder.adapters.CatalogEntryAdapter;
import maestro.bookfinder.model.Catalog;
import maestro.bookfinder.model.CatalogEntry;
import maestro.bookfinder.model.CatalogItem;
import maestro.bookfinder.model.CatalogList;

/**
 * Created by Artyom.Borovsky on 06.10.2015.
 */
public class BooksListFragment extends BaseListFragment<CatalogEntry> implements LoaderManager.LoaderCallbacks<CatalogItem> {

    public static final String TAG = BooksListFragment.class.getSimpleName();

    private static final String PARAM_LIST = "list";
    private static final int LOAD_OFFSET = 6;

    public static BooksListFragment makeInstance(CatalogList list) {
        BooksListFragment fragment = new BooksListFragment();
        Bundle args = new Bundle(1);
        args.putParcelable(PARAM_LIST, list);
        fragment.setArguments(args);
        return fragment;
    }

    private Catalog catalog;
    private CatalogList list;
    private CatalogEntryAdapter mAdapter;
    private boolean isLoading;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        catalog = ((CatalogFragment) getParentFragment()).getCatalog();
        list = getArguments().getParcelable(PARAM_LIST);
        getList().setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!isLoading) {
                    int lastPosition = getList().getChildAdapterPosition(getList().getChildAt(getList().getChildCount() - 1));
                    if (lastPosition >= mAdapter.getItemCount() - 1 - LOAD_OFFSET && lastPosition <= mAdapter.getItemCount() - 1) {
                        getLoaderManager().restartLoader(1, null, BooksListFragment.this);
                    }
                }
            }
        });
        setAdapter(mAdapter = new CatalogEntryAdapter(getContext(), list.getEntries(), getColumnCount()));
        getLoaderManager().initLoader(1, null, this);
    }

    @Override
    public Loader<CatalogItem> onCreateLoader(int id, Bundle args) {
        isLoading = true;
        setFooterProgressVisibility(true);
        return new CatalogListLoader(getContext(), catalog, list);
    }

    @Override
    public void onLoadFinished(Loader<CatalogItem> loader, CatalogItem data) {
        if (data instanceof CatalogList) {
            mAdapter.update(list.getEntries());
        }
        isLoading = false;
    }

    @Override
    public void onLoaderReset(Loader<CatalogItem> loader) {

    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public void onItemClick(CatalogEntry item) {
        getActivity().startActivity(BookInfoActivity.makeInstance(getActivity(), item));
    }

    public static final class CatalogListLoader extends AsyncTaskLoader<CatalogItem> {

        public Catalog catalog;
        public CatalogList list;

        public CatalogListLoader(Context context, Catalog catalog, CatalogList list) {
            super(context);
            this.catalog = catalog;
            this.list = list;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public CatalogItem loadInBackground() {
            return catalog.parse(list);
        }
    }

}