package maestro.bookfinder;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;

import java.util.Arrays;

import maestro.bookfinder.adapters.NavigationAdapter;

public class Main extends AppCompatActivity {

    private DrawerLayout mDrawer;
    private RecyclerView mNavigationList;
    private ActionBarDrawerToggle mDrawerToggle;
    private NavigationAdapter mNavigationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        mDrawer = (DrawerLayout) findViewById(R.id.drawer);
        mNavigationList = (RecyclerView) findViewById(R.id.navigation_list);
        mNavigationList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, R.string.app_name, R.string.app_name);
        mDrawer.setDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mNavigationList.setAdapter(mNavigationAdapter = new NavigationAdapter(getApplicationContext(),
                Arrays.asList(getApplicationContext().getCatalogs())));

        getSupportFragmentManager().beginTransaction().replace(R.id.content_parent, CatalogFragment.makeInstance(getApplicationContext().getCatalogs()[0].getId())).commit();

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(Gravity.START)) {
            mDrawer.closeDrawer(Gravity.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public BookFinderApplication getApplicationContext() {
        return (BookFinderApplication) super.getApplicationContext();
    }

}
