package maestro.bookfinder;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import maestro.bookfinder.model.CatalogEntry;

/**
 * Created by Artyom.Borovsky on 12.10.2015.
 */
public class BookInfoActivity extends AppCompatActivity {

    public static final String TAG = BookInfoActivity.class.getSimpleName();

    private static final String PARAM_ENTRY = "entry";

    public static final Intent makeInstance(Context context, CatalogEntry entry) {
        Intent intent = new Intent(context, BookInfoActivity.class);
        intent.putExtra(PARAM_ENTRY, entry);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent() == null || !getIntent().hasExtra(PARAM_ENTRY)) {
            //TODO: add some error toast?
            finish();
            return;
        }
        if (savedInstanceState == null) {
            Intent intent = getIntent();
            BookInfoDialog.makeInstance(intent.getParcelableExtra(PARAM_ENTRY))
                    .show(getSupportFragmentManager(), BookInfoDialog.TAG);
        }
    }
}
