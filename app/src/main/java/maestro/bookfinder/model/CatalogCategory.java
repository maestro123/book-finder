package maestro.bookfinder.model;

import android.os.Parcel;

/**
 * Created by Artyom.Borovsky on 29.09.2015.
 */
public class CatalogCategory extends CatalogItem {

    private String term;
    private String scheme;

    public CatalogCategory() {

    }

    protected CatalogCategory(Parcel in) {
        super(in);
        term = in.readString();
        scheme = in.readString();
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getTerm() {
        return term;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getScheme() {
        return scheme;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(term);
        dest.writeString(scheme);
    }

    public static final Creator<CatalogCategory> CREATOR = new Creator<CatalogCategory>() {
        @Override
        public CatalogCategory createFromParcel(Parcel source) {
            return new CatalogCategory(source);
        }

        @Override
        public CatalogCategory[] newArray(int size) {
            return new CatalogCategory[size];
        }
    };

}
