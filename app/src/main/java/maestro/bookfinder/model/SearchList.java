package maestro.bookfinder.model;

/**
 * Created by Artyom.Borovsky on 29.09.2015.
 */
public class SearchList {

    private CatalogLink searchItem;
    private CatalogList list;

    public SearchList(CatalogLink item) {
        searchItem = item;
        list = new CatalogList();
    }

}
