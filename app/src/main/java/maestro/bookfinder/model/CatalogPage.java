package maestro.bookfinder.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Artyom on 10/1/2015.
 */
public class CatalogPage extends CatalogItem {

    private ArrayList<CatalogList> lists;
    private ArrayList<CatalogLink> filterLinks;

    public CatalogPage() {

    }

    public CatalogPage(Parcel source) {
        super(source);
        Parcelable[] rLists = source.readParcelableArray(CatalogList.class.getClassLoader());
        if (rLists != null && rLists.length > 0) {
            lists = new ArrayList<>(rLists.length);
            for (Parcelable parcelable : rLists) {
                lists.add((CatalogList) parcelable);
            }
        }
        Parcelable[] rLinks = source.readParcelableArray(CatalogLink.class.getClassLoader());
        if (rLinks != null && rLinks.length > 0) {
            filterLinks = new ArrayList<>(rLinks.length);
            for (Parcelable parcelable : rLinks) {
                filterLinks.add((CatalogLink) parcelable);
            }
        }
    }

    public void addList(CatalogList list) {
        if (lists == null) {
            lists = new ArrayList<>();
        }
        lists.add(list);
    }

    public int getListCount() {
        return lists != null ? lists.size() : 0;
    }

    public CatalogList getList(int position) {
        return lists.get(position);
    }

    public void addFilterLink(CatalogLink link) {
        if (filterLinks == null) {
            filterLinks = new ArrayList<>();
        }
        filterLinks.add(link);
    }

    public boolean haveFilterLinks() {
        return lists != null && lists.size() > 0;
    }

    public ArrayList<CatalogLink> getFilterLinks() {
        return filterLinks;
    }

    public void clear() {
        if (lists != null) {
            lists.clear();
        }
        if (filterLinks != null) {
            filterLinks.clear();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        CatalogList[] wLists = new CatalogList[0];
        if (lists != null && lists.size() > 0) {
            wLists = lists.toArray(new CatalogList[lists.size()]);
        }
        dest.writeParcelableArray(wLists, 0);
        CatalogLink[] wLinks = new CatalogLink[0];
        if (filterLinks != null && filterLinks.size() > 0) {
            wLinks = filterLinks.toArray(new CatalogLink[filterLinks.size()]);
        }
        dest.writeParcelableArray(wLinks, 0);
    }

    public static final Creator<CatalogPage> CREATOR = new Creator<CatalogPage>() {
        @Override
        public CatalogPage createFromParcel(Parcel source) {
            return new CatalogPage(source);
        }

        @Override
        public CatalogPage[] newArray(int size) {
            return new CatalogPage[size];
        }
    };

}
