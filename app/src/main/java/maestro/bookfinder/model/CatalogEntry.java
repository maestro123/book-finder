package maestro.bookfinder.model;

import android.os.Parcel;
import android.text.SpannableStringBuilder;

import java.util.ArrayList;

/**
 * Created by Artyom.Borovsky on 28.09.2015.
 */
public class CatalogEntry extends CatalogLink {

    private String isbn;
    private String published;
    private String updated;
    private String language;
    private String publisher;
    private String issued;
    private ArrayList<String> additionals = new ArrayList<>();
    private ArrayList<CatalogCategory> categories = new ArrayList<>();
    private String image;
    private String thumbnail;
    private ArrayList<CatalogAuthor> authors = new ArrayList<>();
    private ArrayList<CatalogLink> links = new ArrayList<>();
    private CatalogBuyInfo buyInfo;
    private ArrayList<CatalogLink> mDownloadLinks = new ArrayList<>();

    public CatalogEntry() {
    }

    public CatalogEntry(Parcel in) {
        super(in);
        isbn = in.readString();
        published = in.readString();
        updated = in.readString();
        language = in.readString();
        publisher = in.readString();
        issued = in.readString();
        image = in.readString();
        thumbnail = in.readString();
        additionals = in.createStringArrayList();

//        categories = (ArrayList) in.createTypedArrayList(CatalogEntry.CREATOR);
//        authors = (ArrayList) in.createTypedArrayList(CatalogAuthor.CREATOR);
//        links = (ArrayList) in.createTypedArrayList(CatalogLink.CREATOR);

        buyInfo = in.readParcelable(getClass().getClassLoader());
    }

    public void addAuthor(CatalogAuthor author) {
        authors.add(author);
    }

    public int getAuthorsCount() {
        return authors != null ? authors.size() : null;
    }

    public CatalogAuthor getAuthor(int position) {
        return authors.get(position);
    }

    public SpannableStringBuilder getAuthors() {
        if (authors != null) {
            SpannableStringBuilder builder = null;
            for (CatalogAuthor author : authors) {
                if (builder == null) {
                    builder = author.getName();
                } else {
                    builder.append(author.getName());
                }
            }
            return builder;
        }
        return null;
    }

    public void addLink(CatalogLink link) {
        links.add(link);
        if (link.getRel().equals(Catalog.REL_IMAGE)) {
            image = link.getUrl();
        } else if (link.getRel().equals(Catalog.REL_THUMBNAIL)) {
            thumbnail = link.getUrl();
        }
    }

    public int getLinkCount() {
        return links != null ? links.size() : 0;
    }

    public CatalogLink getLink(int position) {
        return links.get(position);
    }

    public void addDownloadLink(CatalogLink link) {
        mDownloadLinks.add(link);
    }

    public int getDownloadLinkCount() {
        return mDownloadLinks != null ? mDownloadLinks.size() : 0;
    }

    public CatalogLink getDownloadLink(int position) {
        return mDownloadLinks.get(position);
    }

    public void addAdditional(String additional) {
        additionals.add(additional);
    }

    public int getAdditionalCount() {
        return additionals != null ? additionals.size() : 0;
    }

    public String getAdditional(int position) {
        return additionals.get(position);
    }

    public void addCategory(CatalogCategory category) {
        categories.add(category);
    }

    public int getCategoriesCount() {
        return categories.size();
    }

    public CatalogCategory getCategory(int position) {
        return categories.get(position);
    }

    public boolean isRedirect() {
        if (links != null) {
            for (CatalogLink link : links) {
                if (link.containsTypes(Catalog.TYPE_APPLICATION_XML)
                        && link.containsTypes(Catalog.TYPE_OPDS_CATALOG)
                        && (link.getRel() == null || link.getRel().contains("http://opds-spec.org") || link.getRel().equals(Catalog.REL_SUBSECTION)))

                    if (Catalog.REL_SUBSECTION.equals(link.getRel()) || link.containsTypes(Catalog.TYPE_KIND_ACQUISITION)) {
                        return true;
                    }
            }
        }
        return false;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setPublished(String published) {
        this.published = published;
    }

    public String getPublished() {
        return published;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getUpdated() {
        return updated;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setIssued(String issued) {
        this.issued = issued;
    }

    public String getIssued() {
        return issued;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setBuyInfo(CatalogBuyInfo buyInfo) {
        this.buyInfo = buyInfo;
    }

    public CatalogBuyInfo getBuyInfo() {
        return buyInfo;
    }

    public CatalogLink getLinkByType(String... types) {
        if (getLinkCount() != 0) {
            for (CatalogLink link : links) {
                if (link.containsTypes(types)) {
                    return link;
                }
            }
        }
        return null;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(isbn);
        dest.writeString(published);
        dest.writeString(updated);
        dest.writeString(language);
        dest.writeString(publisher);
        dest.writeString(issued);
        dest.writeString(image);
        dest.writeString(thumbnail);
        dest.writeStringList(additionals);
//        dest.writeTypedList(categories);
//        dest.writeTypedList(authors);
//        dest.writeTypedList(links);
        dest.writeParcelable(buyInfo, 0);
    }

    public static final Creator<CatalogEntry> CREATOR = new Creator<CatalogEntry>() {
        @Override
        public CatalogEntry createFromParcel(Parcel in) {
            return new CatalogEntry(in);
        }

        @Override
        public CatalogEntry[] newArray(int size) {
            return new CatalogEntry[size];
        }
    };

}
