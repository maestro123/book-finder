package maestro.bookfinder.model;

import android.os.Parcel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Artyom.Borovsky on 29.09.2015.
 */
public class CatalogList extends CatalogEntry {

    private CatalogLink nextPage;

    private ArrayList<CatalogEntry> mCatalogEntries;

    public CatalogList() {

    }

    public CatalogList(String title, String url) {
        setTitle(title);
        setUrl(url);
        setId(url);
    }

    protected CatalogList(Parcel in) {
        super(in);
    }

    public void setNextPage(CatalogLink nextPage) {
        this.nextPage = nextPage;
    }

    public CatalogLink getNextPage() {
        return nextPage;
    }

    public boolean hasNext() {
        return nextPage != null;
    }

    public void add(CatalogEntry entry) {
        if (mCatalogEntries == null) {
            mCatalogEntries = new ArrayList<>();
        }
        mCatalogEntries.add(entry);
    }

    public boolean isEmpty() {
        return mCatalogEntries == null || mCatalogEntries.size() == 0;
    }

    public static final Creator<CatalogList> CREATOR = new Creator<CatalogList>() {
        @Override
        public CatalogList createFromParcel(Parcel in) {
            return new CatalogList(in);
        }

        @Override
        public CatalogList[] newArray(int size) {
            return new CatalogList[size];
        }
    };

    public String getNextPageUrl() {
        return nextPage != null ? nextPage.getUrl() : getUrl();
    }

    public List<CatalogEntry> getEntries() {
        return mCatalogEntries;
    }
}
