package maestro.bookfinder.model;

import android.os.Parcel;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;

/**
 * Created by Artyom.Borovsky on 29.09.2015.
 */
public class CatalogAuthor extends CatalogEntry {

    private String firstName;
    private String lastName;

    public CatalogAuthor() {

    }

    protected CatalogAuthor(Parcel in) {
        super(in);
        firstName = in.readString();
        lastName = in.readString();
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public SpannableStringBuilder getName() {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        if (!TextUtils.isEmpty(firstName)) {
            builder.append(firstName);
        }
        if (!TextUtils.isEmpty(lastName)) {
            builder.append(lastName);
        }
        return builder;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(firstName);
        dest.writeString(lastName);
    }

    @Override
    public String toString() {
        return CatalogAuthor.class.getSimpleName() + " {@" + Integer.toHexString(hashCode()) + "}: "
                + firstName + ", " + lastName;
    }

    public static final Creator<CatalogAuthor> CREATOR = new Creator<CatalogAuthor>() {
        @Override
        public CatalogAuthor createFromParcel(Parcel source) {
            return new CatalogAuthor(source);
        }

        @Override
        public CatalogAuthor[] newArray(int size) {
            return new CatalogAuthor[size];
        }
    };

}
