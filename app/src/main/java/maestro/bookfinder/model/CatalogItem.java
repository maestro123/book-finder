package maestro.bookfinder.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Artyom.Borovsky on 28.09.2015.
 */
public abstract class CatalogItem implements Parcelable {

    private String url;
    private String title;
    private String description;

    public CatalogItem() {

    }

    public CatalogItem(Parcel in) {
        url = in.readString();
        title = in.readString();
        description = in.readString();
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeString(title);
        dest.writeString(description);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof CatalogItem) {
            return ((CatalogItem) o).getUrl().equals(url);
        }
        return super.equals(o);
    }
}
