package maestro.bookfinder.model;

import android.os.Parcel;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Artyom.Borovsky on 29.09.2015.
 */
public class CatalogLink extends CatalogItem {

    private String rel;
    private ArrayList<String> types;
    private String id;

    public CatalogLink() {

    }

    public CatalogLink(Parcel in) {
        super(in);
        rel = in.readString();
        types = in.createStringArrayList();
        id = in.readString();
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    public String getRel() {
        return rel;
    }

    public void setType(String type) {
        types = !TextUtils.isEmpty(type) ? new ArrayList<String>(Arrays.asList(type.split(";"))) : null;
    }

    public boolean containsTypes(String... types) {
        if (types != null) {
            final int size = types.length;
            int containsCount = 0;
            for (String str : this.types) {
                for (String type : types) {
                    if (str.equals(type)) {
                        containsCount++;
                    }
                }
                if (containsCount == size) {
                    return true;
                }
            }
        }
        return false;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(rel);
        dest.writeStringList(types);
        dest.writeString(id);
    }

    public static final Creator<CatalogLink> CREATOR = new Creator<CatalogLink>() {
        @Override
        public CatalogLink createFromParcel(Parcel source) {
            return new CatalogLink(source);
        }

        @Override
        public CatalogLink[] newArray(int size) {
            return new CatalogLink[size];
        }
    };
}
