package maestro.bookfinder.model;

import android.os.Parcel;

/**
 * Created by Artyom.Borovsky on 07.10.2015.
 */
public class CatalogBuyInfo extends CatalogItem {

    private String currency;
    private String price;
    private String type;

    public CatalogBuyInfo() {
    }

    protected CatalogBuyInfo(Parcel in) {
        super(in);
        currency = in.readString();
        price = in.readString();
        type = in.readString();
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(currency);
        dest.writeString(price);
        dest.writeString(type);
    }

    public final static Creator<CatalogBuyInfo> CREATOR = new Creator<CatalogBuyInfo>() {
        @Override
        public CatalogBuyInfo createFromParcel(Parcel source) {
            return new CatalogBuyInfo(source);
        }

        @Override
        public CatalogBuyInfo[] newArray(int size) {
            return new CatalogBuyInfo[size];
        }
    };

}
