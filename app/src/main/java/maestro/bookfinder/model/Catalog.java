package maestro.bookfinder.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import maestro.bookfinder.utils.Utils;

/**
 * Created by Artyom.Borovsky on 28.09.2015.
 */
public class Catalog implements Parcelable {

    public static final String TAG = Catalog.class.getSimpleName();

    private CatalogLink searchLink;
    private HashMap<String, CatalogPage> mCache = new HashMap<>();

    private String homeLink;
    private String title;
    private String imageUrl;
    private String languageCode;
    private String language;
    private String linkPattern;
    private String imagePattern;
    private String baseLink;
    private long id;

    public Catalog() {

    }

    protected Catalog(Parcel in) {
    }

    public void setHomeLink(String homeLink) {
        this.homeLink = homeLink;
    }

    public String getHomeLink() {
        return homeLink;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public String getLanguage() {
        if (language == null && !TextUtils.isEmpty(languageCode)) {
            final Locale[] locales = Locale.getAvailableLocales();
            for (Locale locale : locales) {
                if (locale.getLanguage().equals(languageCode)) {
                    language = locale.getDisplayLanguage();
                    break;
                }
            }
        }
        return language;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static final String REL_BUY = "http://opds-spec.org/acquisition/buy";
    public static final String REL_DOWNLOAD = "http://opds-spec.org/acquisition";
    public static final String REL_IMAGE = "http://opds-spec.org/image";
    public static final String REL_THUMBNAIL = "http://opds-spec.org/image/thumbnail";
    public static final String REL_ALTERNATE = "alternate";
    public static final String REL_SEARCH = "search";
    public static final String REL_SELF = "self";
    public static final String REL_START = "start";
    public static final String REL_PAGE = "application/atom+xml";
    public static final String REL_NEXT_PAGE = "next";
    public static final String REL_SUBSECTION = "subsection";

    public static final String TYPE_IMAGE = "(image/jpeg|image/png|image/gif)";
    public static final String TYPE_APPLICATION_XML = "application/atom+xml";
    public static final String TYPE_KIND_ACQUISITION = "kind=acquisition";
    public static final String TYPE_KIND_NAVIGATION = "kind=navigation";
    public static final String TYPE_OPDS_CATALOG = "profile=opds-catalog";

    private static final String ENTRY = "(entry)";
    private static final String ICON = "(icon)";
    private static final String LINK = "(link)";
    private static final String TYPE = "(type)";
    private static final String TITLE = "(title)";
    private static final String HREF = "(href)";
    private static final String REL = "(rel)";
    private static final String SCHEME = "(scheme)";
    private static final String TERM = "(term)";
    private static final String LABEL = "(label)";
    private static final String ID = "(id)";
    private static final String ISBN = "(dcterms:identifier|dcterms:source)";
    private static final String AUTHOR = "(author)";
    private static final String NAME = "(name|firstName)";
    private static final String LAST_NAME = "(lastName)";
    private static final String PUBLISHED_DATE = "(published)";
    private static final String UPDATED_DATE = "(updated)";
    private static final String PUBLISHER = "(dcterms:publisher)";
    private static final String LANGUAGE = "(dcterms:language)";
    private static final String ISSUED = "(dcterms:issued)";
    private static final String DESCRIPTION = "(summary|content)";
    private static final String ADDITIONAL = "(dcterms:extent)";
    private static final String CATEGORY = "(category)";
    private static final String URI = "(uri)";

    private static final String OPDS_PRICE = "opds:price";
    private static final String CURRENCY_CODE = "currencycode";
    private static final String INDERECT_TYPE = "opds:indirectAcquisition";

    private static final String FORMAT_EPUB = "application/epub+zip";
    private static final String FORMAT_MOBI = "application/x-mobipocket-ebook";
    private static final String FORMAT_PDF = "application/pdf";

    //initial page loading
    public CatalogItem parse(String url) {
        CatalogPage page = new CatalogPage();
        page.setUrl(url);
        page.setTitle(getTitle());
        return parse(page, null);
    }

    public CatalogItem parse(CatalogPage page) {
        return parse(page, null);
    }

    public CatalogItem parse(CatalogList list) {
        return parse(null, list);
    }

    public CatalogItem parse(CatalogPage page, CatalogList catalogList) {
        try {
            String url = null;
            if (page != null) {
                url = page.getUrl();
            } else if (catalogList != null) {
                url = catalogList.getNextPageUrl();
            }
            ArrayList<CatalogItem> mDisplayPage = new ArrayList<>();
            ArrayList<CatalogLink> mFilterPage = new ArrayList<>();
            final boolean isRoot = url.equals(getHomeLink());

            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = factory.newPullParser();
            parser.setInput(new URL(url).openStream(), null);
            CatalogLink currentLink = null;
            CatalogAuthor catalogAuthor = null;
            CatalogEntry catalogEntry = null;
            CatalogBuyInfo buyInfo = null;
            StringBuilder builder = new StringBuilder();
            int nextType;
            while ((nextType = parser.next()) != XmlPullParser.END_DOCUMENT) {
                if (nextType == XmlPullParser.START_TAG) {
                    if (is(ENTRY, parser)) {
                        catalogEntry = new CatalogEntry();
                    } else if (is(AUTHOR, parser)) {
                        catalogAuthor = new CatalogAuthor();
                    } else if (is(LINK, parser)) {
                        currentLink = obtainLink(parser, currentLink);
                    } else if (is(OPDS_PRICE, parser)) {
                        buyInfo = new CatalogBuyInfo();
                        for (int i = 0; i < parser.getAttributeCount(); i++) {
                            String attrName = parser.getAttributeName(i);
                            if (attrName.equals(CURRENCY_CODE)) {
                                buyInfo.setCurrency(parser.getAttributeValue(i));
                            }
                        }
                    } else if (is(INDERECT_TYPE, parser)) {
                        if (buyInfo != null) {
                            final CatalogBuyInfo fBuyInfo = buyInfo;
                            parseAttrs(parser, new OnPairListener() {
                                @Override
                                public void onPair(String tag, String value) {
                                    if (tag.equals(TYPE) && !Utils.isSkipFileType(value)) {
                                        fBuyInfo.setType(value);
                                    }
                                }
                            });
                        }
                    } else if (is(CATEGORY, parser)) {
                        if (catalogEntry != null) {
                            catalogEntry.addCategory(obtainCategory(parser, null));
                        }
                    }
                    builder.setLength(0);
                } else if (nextType == XmlPullParser.END_TAG) {
                    if (is(ENTRY, parser)) {
                        if (buyInfo != null) {
                            catalogEntry.setBuyInfo(buyInfo);
                            buyInfo = null;
                        }
                        if (catalogEntry.isRedirect()) {
                            if (isRoot) {
                                mDisplayPage.add(catalogEntry);
                            } else {
                                mFilterPage.add(catalogEntry);
                            }
                        } else if (catalogList != null) {
                            catalogList.add(catalogEntry);
                        }
                        catalogEntry = null;
                    } else if (is(AUTHOR, parser)) {
                        if (catalogEntry != null) {
                            catalogEntry.addAuthor(catalogAuthor);
                        }
                        catalogAuthor = null;
                    } else if (is(LINK, parser)) {
                        if (currentLink == null) {
                            continue;
                        }
                        if (isSearchLink(currentLink)) {
                            searchLink = currentLink;
                        } else if (REL_START.equals(currentLink.getRel())) {
                            //skip
                        } else if (REL_SELF.equals(currentLink.getRel())) {
                            //skip
                        } else if (catalogEntry != null) {
                            if (REL_DOWNLOAD.equals(currentLink.getRel())) {
                                catalogEntry.addDownloadLink(currentLink);
                            } else {
                                catalogEntry.addLink(currentLink);
                            }
//                        } else if (isRoot && currentLink.containsType(REL_PAGE)) {
//                            mDisplayPage.add(currentLink);
                        } else if (REL_NEXT_PAGE.equals(currentLink.getRel())) {
                            if (catalogList != null)
                                catalogList.setNextPage(currentLink);
                        } else {
                            mFilterPage.add(currentLink);
                        }
                        currentLink = null;
                    } else if (is(OPDS_PRICE, parser)) {
                        if (buyInfo != null) {
                            buyInfo.setPrice(builder.toString());
                        }
                    } else if (catalogAuthor != null) {
                        String text = builder.toString();
                        if (is(NAME, parser)) {
                            catalogAuthor.setFirstName(text);
                        } else if (is(LAST_NAME, parser)) {
                            catalogAuthor.setLastName(text);
                        } else if (is(URI, parser)) {
                            catalogAuthor.setUrl(text);
                        }
                    } else if (catalogEntry != null) {
                        String text = builder.toString();
                        if (is(TITLE, parser)) {
                            catalogEntry.setTitle(text);
                        } else if (is(ID, parser)) {
                            catalogEntry.setId(text);
                        } else if (is(ISBN, parser)) {
                            catalogEntry.setIsbn(text);
                        } else if (is(NAME, parser)) {
                            //?????
                        } else if (is(PUBLISHED_DATE, parser)) {
                            catalogEntry.setPublished(text);
                        } else if (is(UPDATED_DATE, parser)) {
                            catalogEntry.setUpdated(text);
                        } else if (is(PUBLISHER, parser)) {
                            catalogEntry.setPublisher(text);
                        } else if (is(LANGUAGE, parser)) {
                            catalogEntry.setLanguage(text);
                        } else if (is(ISSUED, parser)) {
                            catalogEntry.setIssued(text);
                        } else if (is(DESCRIPTION, parser)) {
                            catalogEntry.setDescription(text);
                        } else if (is(ADDITIONAL, parser)) {
                            catalogEntry.addAdditional(text);
                        }
                    }
                    builder.setLength(0);
                } else if (nextType == XmlPullParser.TEXT) {
                    builder.append(parser.getText());
                }
            }
            Log.e(TAG, "And finally we end!");
            if (catalogList != null) {
                return catalogList;
            } else {
                page.clear();
                for (int i = 0; i < mDisplayPage.size(); i++) {
                    CatalogItem item = mDisplayPage.get(i);
                    CatalogLink link = item instanceof CatalogEntry
                            ? ((CatalogEntry) item).getLinkByType(TYPE_APPLICATION_XML)//, TYPE_KIND_ACQUISITION)
                            : (CatalogLink) item;
                    if (link != null) {
                        page.addList(new CatalogList(item.getTitle(), link.getUrl()));
                    }
                }
                for (int i = 0; i < mFilterPage.size(); i++) {
                    page.addFilterLink(mFilterPage.get(i));
                }
                return page;
            }

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private CatalogLink obtainLink(XmlPullParser parser, CatalogLink link) {
        if (link == null) {
            link = new CatalogLink();
        }
        for (int i = 0; i < parser.getAttributeCount(); i++) {
            String attrName = parser.getAttributeName(i);
            if (is(HREF, attrName)) {
                link.setUrl(buildLink(parser.getAttributeValue(i)));
            } else if (is(REL, attrName)) {
                link.setRel(parser.getAttributeValue(i));
            } else if (is(TYPE, attrName)) {
                link.setType(parser.getAttributeValue(i));
            } else if (is(TITLE, attrName)) {
                link.setTitle(parser.getAttributeValue(i));
            }
        }
        return link;
    }

    private CatalogCategory obtainCategory(XmlPullParser parser, CatalogCategory category) {
        if (category == null) {
            category = new CatalogCategory();
        }
        for (int i = 0; i < parser.getAttributeCount(); i++) {
            String attrName = parser.getAttributeName(i);
            if (is(attrName, SCHEME)) {
                category.setScheme(buildLink(parser.getAttributeValue(i)));
            } else if (is(attrName, TERM)) {
                category.setTerm(parser.getAttributeValue(i));
            } else if (is(attrName, LABEL)) {
                category.setTitle(parser.getAttributeType(i));
            }
        }
        return category;
    }

    private String buildLink(String link) {
        if (TextUtils.isEmpty(link)) {
            return link;
        }
        if (link.contains("://")) {
            return link;
        }
        if (baseLink == null) {
            if (TextUtils.isEmpty(homeLink)) {
                return null;
            }
            StringBuilder builder = new StringBuilder();
            String workLink = homeLink;
            int index = workLink.indexOf("://");
            if (index != -1) {
                workLink = workLink.substring(index);
                builder.append("http://");
            }
            index = workLink.indexOf("/");
            builder.append(workLink.substring(0, index));
            baseLink = builder.toString();
        }
        return String.format("%s%s", baseLink, link);
    }

    @Override
    public String toString() {
        return "Catalog (@" + Integer.toHexString(hashCode()) + ") : " + title;
    }

    public static boolean is(String tag, XmlPullParser parser) {
        return is(tag, parser.getName());
    }

    public static boolean is(String tag, String param) {
        if (TextUtils.isEmpty(tag) || TextUtils.isEmpty(param)) {
            return false;
        }
        return param.matches(tag);
    }

    private static final boolean isSearchLink(CatalogLink item) {
        return item.getRel().equals(REL_SEARCH);
    }

    public interface OnPairListener {
        void onPair(String tag, String value);
    }

    public static void parseAttrs(XmlPullParser parser, OnPairListener listener) {
        if (listener != null && parser != null) {
            for (int i = 0; i < parser.getAttributeCount(); i++) {
                listener.onPair(parser.getAttributeName(i), parser.getAttributeValue(i));
            }
        }
    }

}
