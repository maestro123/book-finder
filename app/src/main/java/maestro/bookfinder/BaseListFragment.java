package maestro.bookfinder;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import maestro.bookfinder.adapters.BaseItemAdapter;

/**
 * Created by Artyom.Borovsky on 06.10.2015.
 */
public abstract class BaseListFragment<ItemType> extends Fragment implements BaseItemAdapter.OnItemClickListener<ItemType> {

    public static final String TAG = BaseListFragment.class.getSimpleName();

    private RecyclerView list;
    private BaseItemAdapter mAdapter;

    private final GridLayoutManager.SpanSizeLookup mLookUp = new GridLayoutManager.SpanSizeLookup() {
        @Override
        public int getSpanSize(int position) {
            return mAdapter.getSpanCount(position, getColumnCount());
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(getLayoutId(), null);
        list = (RecyclerView) v.findViewById(android.R.id.list);
        prepareAdapter();
        return v;
    }

    public void prepareAdapter() {
        if (getColumnCount() > 1) {
            GridLayoutManager manager = new GridLayoutManager(getContext(), getColumnCount());
            manager.setSpanSizeLookup(mLookUp);
            list.setLayoutManager(new GridLayoutManager(getContext(), getColumnCount()));
        } else {
            list.setLayoutManager(new LinearLayoutManager(getContext()));
        }
    }

    public int getLayoutId() {
        return R.layout.list_fragment_view;
    }

    public RecyclerView getList() {
        return list;
    }

    public void setAdapter(BaseItemAdapter adapter) {
        mAdapter = adapter;
        list.setAdapter(adapter);
        if (mAdapter != null) {
            mAdapter.setOnItemClickListener(this);
        }
    }

    public int getColumnCount() {
        return 1;
    }

    public void setFooterProgressVisibility(boolean visible) {
        Log.e(TAG, "setFooterProgressVisibility: " + visible);
        if (mAdapter != null) {
            mAdapter.setShowFooterProgress(visible);
        }
    }
}
