package maestro.bookfinder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import maestro.bookfinder.adapters.BaseItemAdapter;
import maestro.bookfinder.model.CatalogLink;

/**
 * Created by Artyom.Borovsky on 06.10.2015.
 */
public class FiltersFragment extends BaseListFragment<CatalogLink> {

    public static final String TAG = FiltersFragment.class.getSimpleName();

    private static final String PARAM_LINKS = "links";

    public static final FiltersFragment makeInstance(ArrayList<CatalogLink> links) {
        FiltersFragment fragment = new FiltersFragment();
        Bundle args = new Bundle(1);
        args.putParcelableArrayList(PARAM_LINKS, links);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setAdapter(new FiltersAdapter(getContext(), getArguments().<CatalogLink>getParcelableArrayList(PARAM_LINKS)));
    }

    @Override
    public void onItemClick(CatalogLink item) {

    }

    public static final class FiltersAdapter extends BaseItemAdapter<CatalogLink> {

        public FiltersAdapter(Context context, List<CatalogLink> items) {
            super(context, items);
        }

        @Override
        public Holder onCreateHolder(int viewType) {
            return new Holder(inflate(R.layout.single_line_item));
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        public class Holder extends BaseTypeHolder<CatalogLink> {

            TextView Title;

            public Holder(View v) {
                super(v);
                Title = (TextView) v.findViewById(R.id.title);
            }

            @Override
            public void onBind(CatalogLink item) {
                Title.setText(item.getTitle());
            }

        }

    }

}
