package maestro.bookfinder;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import maestro.bookfinder.model.CatalogEntry;

/**
 * Created by Artyom.Borovsky on 12.10.2015.
 */
public class BookInfoDialog extends DialogFragment implements ViewTreeObserver.OnScrollChangedListener {

    public static final String TAG = BookInfoDialog.class.getSimpleName();

    private static final String PARAM_ENTRY = "entry";

    public static final BookInfoDialog makeInstance(Parcelable entry) {
        BookInfoDialog dialog = new BookInfoDialog();
        Bundle args = new Bundle(1);
        args.putParcelable(PARAM_ENTRY, entry);
        dialog.setArguments(args);
        return dialog;
    }

    private ScrollView mScrollView;
    private ImageView mImage;
    private ImageView mBackground;
    private TextView mTitle;
    private TextView mAuthor;
    private TextView mContent;

    private CatalogEntry mEntry;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEntry = getArguments().getParcelable(PARAM_ENTRY);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.book_info_view, null);
        mScrollView = (ScrollView) v.findViewById(R.id.scroll_view);
        mImage = (ImageView) v.findViewById(R.id.image);
        mBackground = (ImageView) v.findViewById(R.id.background_image);
        mTitle = (TextView) v.findViewById(R.id.title);
        mAuthor = (TextView) v.findViewById(R.id.author);
        mContent = (TextView) v.findViewById(R.id.content);

        mTitle.setText(mEntry.getTitle());
        mAuthor.setText(mEntry.getAuthors());
        mContent.setText(mEntry.getDescription());

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Picasso.with(getActivity()).load(mEntry.getImage()).into(mImage);
        Picasso.with(getActivity()).load(mEntry.getImage()).fit().transform(new Transformation() {

            private int mRadius = 25;

            @Override
            public Bitmap transform(Bitmap source) {
                if (Build.VERSION.SDK_INT < 17) {
                    return source;
                }

                RenderScript rs = RenderScript.create(getActivity());
                Allocation input = Allocation.createFromBitmap(rs, source, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
                Allocation output = Allocation.createTyped(rs, input.getType());
                ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
                script.setRadius(mRadius);
                script.setInput(input);
                script.forEach(output);
                output.copyTo(source);
                return source;
            }

            @Override
            public String key() {
                return "blur15";
            }

        }).into(mBackground);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (getActivity() instanceof BookInfoActivity) {
            getActivity().finish();
        }
    }

    @Override
    public void onScrollChanged() {

    }
}
