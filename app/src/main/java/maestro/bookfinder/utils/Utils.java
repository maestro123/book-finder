package maestro.bookfinder.utils;

import android.text.TextUtils;

/**
 * Created by Artyom.Borovsky on 29.09.2015.
 */
public class Utils {

    private static final String FILE_TYPE_VND_ADOBE_XML = "application/vnd.adobe.adept+xml";

    public static boolean isSkipFileType(String fileType) {
        return TextUtils.isEmpty(fileType) || fileType.equals(FILE_TYPE_VND_ADOBE_XML);
    }

}
