package maestro.bookfinder.utils;

import android.os.Handler;

import java.util.ArrayList;

/**
 * Created by Artyom.Borovsky on 13.10.2015.
 */
public class DownloadItem {

    public static final Handler uiHandler = new Handler();

    private static final long NOTIFY_DELAY = 500;

    String outPath;
    String downloadUrl;
    Object parentObject;
    ArrayList<DownloadManager.DownloadListener> mListeners = new ArrayList<>();

    private Thread mNotifyThread;

    int progress;
    int previousProgress;

    public DownloadItem() {
    }

    public void setOutPath(String outPath) {
        this.outPath = outPath;
    }

    public void setParentObject(Object parentObject) {
        this.parentObject = parentObject;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public void attachListener(DownloadManager.DownloadListener listener) {
        synchronized (mListeners) {
            mListeners.add(listener);
        }
    }

    public void detachListener(DownloadManager.DownloadListener listener) {
        synchronized (mListeners) {
            mListeners.remove(listener);
        }
    }

    void startNotifyThread() {
        if (mNotifyThread == null) {
            mNotifyThread = new Thread() {
                @Override
                public void run() {
                    super.run();
                    while (!isInterrupted()) {
                        if (progress != previousProgress) {
                            previousProgress = progress;
                            uiHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    synchronized (mListeners) {
                                        for (DownloadManager.DownloadListener listener : mListeners) {
                                            listener.onDownloadProgress(DownloadItem.this, previousProgress);
                                        }
                                    }
                                }
                            });
                        }
                        try {
                            sleep(NOTIFY_DELAY);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            };
            mNotifyThread.start();
        }
    }

    void stopNotifyThread() {
        if (mNotifyThread != null) {
            mNotifyThread.interrupt();
        }
    }

    void notifyStart(){

    }

    void notifyEnd(){

    }

}
