package maestro.bookfinder.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.TypedValue;

import com.squareup.picasso.Transformation;

/**
 * Created by Artyom.Borovsky on 13.10.2015.
 */
public class CoverTransformation implements Transformation {

    private final int color = 0xff424242;
    private final float radius;

    public CoverTransformation(Context context) {
        radius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, context.getResources().getDisplayMetrics());
    }

    @Override
    public Bitmap transform(Bitmap source) {

        Bitmap out = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(out);

        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, source.getWidth(), source.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, radius, radius, paint);

        Rect rectBR = new Rect(source.getWidth() / 2, source.getHeight() / 2, source.getWidth(), source.getHeight());
        canvas.drawRect(rectBR, paint);
        Rect rectBL = new Rect(0, source.getHeight() / 2, source.getWidth() / 2, source.getHeight());
        canvas.drawRect(rectBL, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(source, rect, rect, paint);
        source.recycle();
        return out;
    }

    @Override
    public String key() {
        return "round";
    }

}
