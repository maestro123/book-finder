package maestro.bookfinder.utils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by Artyom.Borovsky on 13.10.2015.
 */
public class DownloadManager {

    private static volatile DownloadManager instance;

    public static synchronized DownloadManager getInstance() {
        return instance != null ? instance : (instance = new DownloadManager());
    }

    private Thread mDownloadThread;
    private HashMap<String, DownloadItem> mItems = new HashMap<>();

    public void add(String key, DownloadItem item) {
        synchronized (mItems) {
            if (!mItems.containsKey(key)) {
                mItems.put(key, item);
                next();
            }
        }
    }

    public DownloadItem get(String key) {
        synchronized (mItems) {
            return mItems.get(key);
        }
    }

    private final void next() {
        if (mDownloadThread == null) {
            mDownloadThread = new Thread() {
                @Override
                public void run() {
                    super.run();
                    while (!isInterrupted()) {
                        String key;
                        DownloadItem item;
                        synchronized (mItems) {
                            key = mItems.keySet().iterator().next();
                            item = mItems.get(key);
                        }

                        try {
                            FileOutputStream outStream = new FileOutputStream(item.outPath);
                            InputStream inStream = new URL(item.downloadUrl).openStream();

                            int available = inStream.available();
                            boolean isProgressNotifyEnable = available > 1 * 1000 * 1000;

                            byte[] buffer = new byte[1024];

                            while (inStream.read(buffer) != -1) {
                                outStream.write(buffer);
                            }

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        synchronized (mItems) {
                            mItems.remove(key);
                        }

                        if (mItems.size() == 0) {
                            try {
                                wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            };
            mDownloadThread.start();
        } else {
            mDownloadThread.notify();
        }
    }

    public interface DownloadListener {
        void onDownloadStart(DownloadItem item);

        void onDownloadProgress(DownloadItem item, int progress);

        void onDownloadEnd(DownloadItem item);

        void onDownloadError();
    }

    public static class SimpleOnDownloadListener implements DownloadListener{

        @Override
        public void onDownloadStart(DownloadItem item) {

        }

        @Override
        public void onDownloadProgress(DownloadItem item, int progress) {

        }

        @Override
        public void onDownloadEnd(DownloadItem item) {

        }

        @Override
        public void onDownloadError() {

        }
    }

}
