package maestro.bookfinder;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Artyom.Borovsky on 12.10.2015.
 */
public class TagsView extends ViewGroup {

    public static final String TAG = TagsView.class.getSimpleName();

    private ArrayList<Tag> mTags = new ArrayList<>();

    private int mPadding;

    private boolean isReorderRequired;

    public TagsView(Context context) {
        super(context);
        init(null);
    }

    public TagsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public TagsView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private final void init(AttributeSet attrs) {
        mPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
        if (attrs != null) {

        }
        setBackgroundColor(Color.parseColor("#4acacaca"));
        setPadding(mPadding, mPadding, mPadding, mPadding);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        final int childCount = getChildCount();
        if (childCount == 0) return;

        final int width = getWidth() - getPaddingLeft() - getPaddingRight();
        final int height = getHeight() - getPaddingBottom() - getPaddingTop();

        int left = getPaddingLeft();
        int top = getPaddingTop();
        int right = getPaddingRight();

        for (int i = 0; i < childCount; i++) {
            final View child = getChildAt(i);
            int cWidth = child.getMeasuredWidth();
            int cHeight = child.getMeasuredHeight();
            if (left + cWidth + mPadding > width) {
                top += cHeight + mPadding;
                left = getPaddingLeft();
                if (cWidth + mPadding > width) {
                    child.layout(left, top, left + width, top + cHeight);
                } else {
                    child.layout(left, top, Math.min(left + cWidth, left + width), top + cHeight);
                }
            } else {
                child.layout(left, top, left + cWidth, top + cHeight);
                left += cWidth + mPadding;
            }
        }

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int widthSpec = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int heightSpec = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        final int widthMax = MeasureSpec.getSize(widthMeasureSpec);
        final int heightMax = MeasureSpec.getSize(heightMeasureSpec);
        final int childCount = getChildCount();

        measureChildren(widthMeasureSpec, heightMeasureSpec);

        if (isReorderRequired == true) {
            //TODO: add reorder logic for optimisation space usage
        }

        if (widthMeasureSpec == MeasureSpec.EXACTLY && heightMeasureSpec == MeasureSpec.EXACTLY) {
            setMeasuredDimension(widthSpec, heightSpec);
            return;
        }

        int maxWidth = 0;
        int maxHeight = 0;

        int left = getPaddingLeft();
        int top = getPaddingTop();
        int right = getPaddingRight();

        for (int i = 0; i < childCount; i++) {
            final View child = getChildAt(i);
            int cWidth = child.getMeasuredWidth();
            int cHeight = child.getMeasuredHeight();

            if (left + cWidth + mPadding > widthMax - right) {
                top += cHeight + mPadding;
                left = getPaddingLeft();
                if (cWidth + mPadding > widthMax - right) {
                    maxWidth = Math.max(maxWidth, cWidth);
                } else {
                    maxWidth = Math.max(maxWidth, left);
                }
                left += cWidth;
            } else {
                left += cWidth + mPadding;
                maxWidth = Math.max(maxWidth, left);
                if (i == 0) {
                    top += cHeight + mPadding;
                }
            }
        }
        maxWidth += right;
        maxHeight = top;

        int width = 0;
        int height = 0;

        if (widthMeasureSpec == MeasureSpec.EXACTLY) {
            width = widthSpec;
        } else if (widthMeasureSpec == MeasureSpec.AT_MOST) {
            width = Math.max(widthMax, maxWidth);
        } else {
            width = maxWidth;
        }

        if (heightMeasureSpec == MeasureSpec.EXACTLY) {
            height = heightSpec;
        } else if (heightMeasureSpec == MeasureSpec.AT_MOST) {
            height = Math.max(heightMax, maxHeight);
        } else {
            height = maxHeight;
        }

        setMeasuredDimension(width, height);
    }

    public void addTag(String text) {
        addTag(new Tag(text));
    }

    public void addTag(Tag tag) {
        if (!mTags.contains(tag)) {
            isReorderRequired = true;
            addView(makeTagView(tag));
        }
    }

    private final View makeTagView(Tag tag) {
        TextView textView = new TextView(getContext());
        textView.setText(tag.text);
        textView.setBackgroundColor(tag.color);
        textView.setSingleLine();
        textView.setTextSize(14);
        textView.setPadding(mPadding, mPadding / 2, mPadding, mPadding / 2);
        return textView;
    }

    public static final class Tag {

        private String text;
        private int color = Color.TRANSPARENT;

        public Tag() {
        }

        public Tag(String text) {
            this.text = text;
        }

        public Tag(String text, int color) {
            this.text = text;
            this.color = color;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public int getColor() {
            return color;
        }

        public void setColor(int color) {
            this.color = color;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) {
                return true;
            } else if (o instanceof Tag && !TextUtils.isEmpty(((Tag) o).text)) {
                return ((Tag) o).text.equals(text);
            }
            return super.equals(o);
        }
    }

}
